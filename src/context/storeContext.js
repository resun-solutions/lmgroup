import React from "react"

const defaultState = {
  cart: [],
  addToCart: () => {},
  viewItemId: -1,
  emailPopupOpen: false,
  galleryOpen: false,
  galleryItemId: -1,
  isRussian: false,
  isEnglish: false,
  toggleGallery: () => {},
  setGalleryItemId: () => {},
  toggleEmailPopup: () => {}
}

const StoreContext = React.createContext(defaultState)

class StoreProvider extends React.Component {
  state = {
    cart: [],
    viewItemId: -1,
    galleryOpen: false,
    galleryItemId: -1,
    emailPopupOpen: false
  }
  viewItem = (id) => {
    let tmp = this.state.viewItemId;
    tmp = id;
    this.setState({viewItemId: tmp})
  }

  addToCart = (id, data, amount=1) => {
    //cid = category id
    let cart = this.state.cart;
    let addedToCart = false;

    for (let index = 0; index < cart.length; index++) {
      const element = cart[index];
      if(element["id"] == id) {
        cart[index]["amount"]+=amount
        addedToCart = true;
      }
    }

    if(!addedToCart) {
      cart.push({"id":id,"amount":amount,"data":data});
    }

    localStorage.setItem("cart", JSON.stringify(cart))
    this.setState({ cart })
    alert("Produkts pievienots grozam!")
  }

  toggleEmailPopup = () => {
    let emailPopupOpen = this.state.emailPopupOpen

    if (window.location.href.indexOf("ru") > -1) {
      this.setState({isRussian: true});
    }
    else if (window.location.href.indexOf("en") > -1) {
      this.setState({isEnglish: true});
    }else {
      this.setState({isRussian: false});
      this.setState({isEnglish: false});
    }

    this.setState({emailPopupOpen: !emailPopupOpen})
  }

  toggleGallery = () => {
    let galleryOpen = this.state.galleryOpen
    this.setState({galleryOpen: !galleryOpen})
  }

  setGalleryItemId = (id) => {
    this.setState({galleryItemId: id})
  }

  componentDidMount() {
    const savedCart = JSON.parse(localStorage.getItem("cart"))
    if (savedCart) {
      this.setState({ cart: savedCart })
    }
  }

  render() {
    const { children } = this.props
    const { cart, viewItemId, emailPopupOpen, galleryOpen, galleryItemId, isRussian, isEnglish } = this.state
    return (
      <StoreContext.Provider
        value={{
          cart,
          viewItemId,
          addToCart: this.addToCart,
          emailPopupOpen,
          toggleEmailPopup: this.toggleEmailPopup,
          viewItem: this.viewItem,
          galleryOpen,
          toggleGallery: this.toggleGallery,
          galleryItemId,
          isRussian,
          isEnglish,
          setGalleryItemId: this.setGalleryItemId
        }}
      >
        {children}
      </StoreContext.Provider>
    )
  }
}

export default StoreContext
export { StoreProvider }