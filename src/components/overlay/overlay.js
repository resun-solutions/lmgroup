import React from "react"
import "./style.scss"

const Overlay = ({visible}) => {

    return (
        <div className={"pen overlay-component fixed t0 l0 w-100 h-100 state-"+visible}></div>
    )

}

export default Overlay