import React from "react"
import { Link } from "gatsby"
import "./action-banner.scss"
import { Parallax } from 'react-parallax';
import StoreContext from "../../context/storeContext"

const ActionBanner = ({url, title, subtitle, cta, href}) => {
// /pakalpojumi/#kontakti

if(href == "email") {
  return (
    <StoreContext.Consumer>
      {
        storeData => (
          <div id="action-banner">
            <Parallax bgImage={require('../../images/'+url)} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={400}>
                <div className="landing-tint"></div>
                <div className="flex flex-column items-center justify-center white absolute t0 w-100 h-100">
                <h2 className="ttu ms-bold f3 f1-ns tc mb1">{title}</h2>

                <p className="mb4 f6 f4-m f5-l w-80 tc">{subtitle}</p>

                  <button onClick={storeData.toggleEmailPopup} className="button transparent button--empty cp white pv3 ph4 relative">{cta}</button>
                </div>
            </Parallax>
          </div>
        )
      }
    </StoreContext.Consumer>
  )
}else {
  return (
    <div id="action-banner">
      <Parallax bgImage={require('../../images/'+url)} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={400}>
          <div className="landing-tint"></div>
          <div className="flex flex-column items-center justify-center white absolute t0 w-100 h-100">
          <h2 className="ttu ms-bold f3 f1-ns tc mb1">{title}</h2>

          <p className="mb4 f6 f4-m f5-l w-80 tc">{subtitle}</p>

          <Link className="button button--empty cp white pv3 ph4 relative" to={href}>{cta}</Link>
          </div>
      </Parallax>
    </div>
  )
}
}

export default ActionBanner