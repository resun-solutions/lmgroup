/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import MiniNavbar from "./navbar/mini-navbar"
import Copyright from "./footer/copyright"
import MailForm from "./footer/mail"
import "./layout.css"
import Navbar from "./navbar/navbar"
import NavbarRu from "./navbar/navbar.ru.js"
import NavbarEn from "./navbar/navbar.en.js"

class Layout extends React.Component {

constructor(props) {
  super(props);
}

render () {
  console.log(this.refs);

  if(this.props.minimal) {
    return (
      <>
        <main>{this.props.children}</main>
      </>
    )
  }

  if(this.props.ru) {
    return (
      <>
      <MiniNavbar></MiniNavbar>
      <NavbarRu></NavbarRu>
      <main>{this.props.children}</main>
      <MailForm ru="true" />
      <Copyright ru="true" />
    </>
    )
  }

  if(this.props.en) {
    return (
      <>
      <MiniNavbar></MiniNavbar>
      <NavbarEn></NavbarEn>
      <main>{this.props.children}</main>
      <MailForm en="true" />
      <Copyright en="true" />
    </>
    )
  }

  return (
    <>
      <MiniNavbar></MiniNavbar>
      <Navbar></Navbar>
      <main>{this.props.children}</main>
      <MailForm />
      <Copyright />
    </>
  )
}

}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout