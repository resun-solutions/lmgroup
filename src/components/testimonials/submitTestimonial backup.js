import React from "react";
import Header from "../typography/header"
import "./testimonials.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'

import Reviews from '../../../data/testimonials.json'

library.add(fab)


class SubmitTestimonialBCK extends React.Component {
    constructor(props) {
        super(props);
        this.formStatus = -1; // -1 unsent | 500 error | 200 okay | 0 waiting

        this.visibleReviewId = 0;
        this.reviewSize = Reviews.length;
        setInterval(this.rotateReviews, 4000);
    }

    sendMail = (e) => {
        this.formStatus = 0;
        this.forceUpdate();
        
        e.preventDefault();

        let proxyUrl = 'https://cors-anywhere.herokuapp.com/';
        let targetUrl = 'https://chalete.lv/es/index.php';

        let formData = new FormData(document.getElementById("testimonial_form"));
        
        fetch(proxyUrl + targetUrl, {
        method: "POST",
        body: formData
        })
        .then(res => {
            return res.text();
        })
        .then(data => {
            this.formStatus = data;


            this.forceUpdate();

            if(this.formStatus == '500') {
                setTimeout(() =>{
                    this.formStatus = -1;
                    this.forceUpdate();
                },5000);
            }
        });
    }

    rotateReviews = () => {
        let allReviews = document.querySelectorAll(".review-item");

        for(var j = 0, length2 = allReviews.length; j < length2; j++){

            if(allReviews[j]!=undefined) {
                allReviews[j].classList.add("review-item--hidden");
            }
        }


        this.visibleReviewId+=1;
        if(this.visibleReviewId > this.reviewSize-1) {
            this.visibleReviewId = 0;
        }

        // console.log(allReviews[1]);
        if(allReviews[this.visibleReviewId]!=undefined) {
            allReviews[this.visibleReviewId].classList.remove("review-item--hidden");
        }
    }

    render() {
     
        if(this.props.ru) {
            if(this.formStatus == '200') {

            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Оставьте нам свой отзыв</Header>
                        <p className="pb4 pb5-ns db pr5-l">
                            Обратная связь важна для любого бизнеса, потому что это один из способов, которыми мы можем развивать и улучшать качество наших услуг. Именно поэтому - если вы чем-то не удовлетворены или, наоборот, удивлены и восхищены, поделитесь!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                 <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center flex justify-center pt6-l tc">
                        <div className="success-message-1">
                            <p className="f5 ph5 pv4 sec-font fwn white lh1 ma0">Спасибо! Сообщение и отправлено :)</p>
                        </div>
                    </div>
                </div>
            )

            }else if(this.formStatus == '500') {
                return (
                    <div className="flex flex-column flex-row-l">
                        <div className="w-100 w-50-l">
                            <Header color="yellow" showLine>Оставьте нам свой отзыв</Header>
                            <p className="pb4 pb5-ns db pr5-l">
                                Обратная связь важна для любого бизнеса, потому что это один из способов, которыми мы можем развивать и улучшать качество наших услуг. Именно поэтому - если вы чем-то не удовлетворены или, наоборот, удивлены и восхищены, поделитесь!
                            </p>

                            {
                                Reviews.map((item, index) => {

                                    return (
                                     <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                        <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                            <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                        </a>

                                        <p className="mb0 pa3 grow pen">
                                            <i className="o-80">
                                            {item.review}
                                            </i> <br/>
                                        </p>
                                    </div> 
                                    )   
                                })
                            }
                        </div>
                        <div className="w-100 w-50-l items-center flex justify-center pt6-l tc">
                            <div className="error-message-1">
                                <p className="f5 fwn ph5 pv4 sec-font white lh1 ma0">Что-то пошло не так :(</p>
                            </div>
                        </div>
                    </div>
                )
            }

            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Оставьте нам свой отзыв</Header>
                        <p className="db pr5-l">
                            Обратная связь важна для любого бизнеса, потому что это один из способов, которыми мы можем развивать и улучшать качество наших услуг. Именно поэтому - если вы чем-то не удовлетворены или, наоборот, удивлены и восхищены, поделитесь!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                 <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center pt6-l">
                        <form id="testimonial_form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                            <div className="input-group flex flex-column">
                                <label htmlFor="name">Ваше имя</label>
                                <input className="input-reset mb3 pv2 ph3" type="text" name="name" id="name" placeholder="Jānis Bērziņš" id=""/>
                            </div>
                            <div className="input-group flex flex-column">
                                <label htmlFor="email">Ваш электронный адрес</label>
                                <input className="input-reset mb3 pv2 ph3" type="email" name="email" id="email" placeholder="janis@gmail.com" id=""/>
                            </div>
                            <div className="input-group flex flex-column">
                                <label htmlFor="sender_testimonial">Oтзыв</label>
                                <textarea className="mb3 pv2 ph3" name="testimonial" id="" rows="5" id="sender_testimonial" placeholder="..."></textarea>
                            </div>

                             {/*<input className="dn" type="text" name="honey" value="" /> */}
                            <input className="dn" type="text" name="ajax" value="true" hidden />

                            <div className="input-group">
                                <button onClick={(e) =>{this.sendMail(e)}} type="submit" className={"fr button button--blue cp br-10 pv3 ph4 relative dib"+(this.formStatus==0?' o-20 pen':'')} to="/projekti/#kontakti">Продолжать</button>
                            </div>
                        </form>
                    </div>
                </div>
            )
        }

    if(this.props.en) {
            if(this.formStatus == '200') {

            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Leave your feedback</Header>
                        <p className="pb4 pb5-ns db pr5-l">
                            Feedback is important for any business because it is one of the ways we can develop and improve the quality of our services. That is why - if you are not satisfied with something or, on the contrary, surprised and delighted, share it!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                 <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center flex justify-center pt6-l tc">
                        <div className="success-message-1">
                            <p className="f5 ph5 pv4 sec-font fwn white lh1 ma0">Thanks! Message and sent :)</p>
                        </div>
                    </div>
                </div>
            )

            }else if(this.formStatus == '500') {
                return (
                    <div className="flex flex-column flex-row-l">
                        <div className="w-100 w-50-l">
                            <Header color="yellow" showLine>Leave your feedback</Header>
                            <p className="pb4 pb5-ns db pr5-l">
                                Feedback is important for any business because it is one of the ways we can develop and improve the quality of our services. That is why - if you are not satisfied with something or, on the contrary, surprised and delighted, share it!
                            </p>

                            {
                                Reviews.map((item, index) => {

                                    return (
                                     <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                        <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                            <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                        </a>

                                        <p className="mb0 pa3 grow pen">
                                            <i className="o-80">
                                            {item.review}
                                            </i> <br/>
                                        </p>
                                    </div> 
                                    )   
                                })
                            }
                        </div>
                        <div className="w-100 w-50-l items-center flex justify-center pt6-l tc">
                            <div className="error-message-1">
                                <p className="f5 fwn ph5 pv4 sec-font white lh1 ma0">Something went wrong :(</p>
                            </div>
                        </div>
                    </div>
                )
            }

            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Leave your feedback</Header>
                        <p className="db pr5-l">
                            Feedback is important for any business because it is one of the ways we can develop and improve the quality of our services. That is why - if you are not satisfied with something or, on the contrary, surprised and delighted, share it!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                 <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center pt6-l">
                        <form id="testimonial_form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                            <div className="input-group flex flex-column">
                                <label htmlFor="name">Your name</label>
                                <input className="input-reset mb3 pv2 ph3" type="text" name="name" id="name" placeholder="Jānis Bērziņš" id=""/>
                            </div>
                            <div className="input-group flex flex-column">
                                <label htmlFor="email">Your email</label>
                                <input className="input-reset mb3 pv2 ph3" type="email" name="email" id="email" placeholder="janis@gmail.com" id=""/>
                            </div>
                            <div className="input-group flex flex-column">
                                <label htmlFor="sender_testimonial">Feedback</label>
                                <textarea className="mb3 pv2 ph3" name="testimonial" id="" rows="5" id="sender_testimonial" placeholder="..."></textarea>
                            </div>

                            {/*<input className="dn" type="text" name="honey" value="" /> */}
                            <input className="dn" type="text" name="ajax" value="true" hidden />

                            <div className="input-group">
                                <button onClick={(e) =>{this.sendMail(e)}} type="submit" className={"fr button button--blue cp br-10 pv3 ph4 relative dib"+(this.formStatus==0?' o-20 pen':'')} to="/projekti/#kontakti">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            )
        }



        if(this.formStatus == '200') {

            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Atstāj mums savu atsauksmi</Header>
                        <p className="pb4 pb5-ns db pr5-l">
                            Jebkuram uzņēmumam atsauksmes ir svarīgas, jo tas ir viens no veidiem, kā varam attīstīties un uzlabot savu pakalpjumu kvalitāti. Tieši tāpēc - Ja tevi kaut kas neapmierināja vai tieši otrādāk, pārsteidza un iepriecināja, padalies!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                 <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center flex justify-center pt6-l tc">
                        <div className="success-message-1">
                            <p className="f5 ph5 pv4 sec-font fwn white lh1 ma0">Paldies! Ziņa ir nosūtīta :)</p>
                        </div>
                    </div>
                </div>
            )

        }else if(this.formStatus == '500') {
            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Atstāj mums savu atsauksmi</Header>
                        <p className="pb4 pb5-ns db pr5-l">
                            Jebkuram uzņēmumam atsauksmes ir svarīgas, jo tas ir viens no veidiem, kā varam attīstīties un uzlabot savu pakalpjumu kvalitāti. Tieši tāpēc - Ja tevi kaut kas neapmierināja vai tieši otrādāk, pārsteidza un iepriecināja, padalies!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                 <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center flex justify-center pt6-l tc">
                        <div className="error-message-1">
                            <p className="f5 fwn ph5 pv4 sec-font white lh1 ma0">Kaut kas nogāja greizi :(</p>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-50-l">
                    <Header color="yellow" showLine>Atstāj mums savu atsauksmi</Header>
                    <p className="db pr5-l">
                        Jebkuram uzņēmumam atsauksmes ir svarīgas, jo tas ir viens no veidiem, kā varam attīstīties un uzlabot savu pakalpjumu kvalitāti. Tieši tāpēc - Ja tevi kaut kas neapmierināja vai tieši otrādāk, pārsteidza un iepriecināja, padalies!
                    </p>

                    {
                        Reviews.map((item, index) => {

                            return (
                             <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                    <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                </a>

                                <p className="mb0 pa3 grow pen">
                                    <i className="o-80">
                                    {item.review}
                                    </i> <br/>
                                </p>
                            </div> 
                            )   
                        })
                    }
                </div>
                <div className="w-100 w-50-l items-center pt6-l">
                    <form id="testimonial_form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                        <div className="input-group flex flex-column">
                            <label htmlFor="name">Tavs vārds</label>
                            <input className="input-reset mb3 pv2 ph3" type="text" name="name" id="name" placeholder="Jānis Bērziņš" id=""/>
                        </div>
                        <div className="input-group flex flex-column">
                            <label htmlFor="email">Tavs e-pasts</label>
                            <input className="input-reset mb3 pv2 ph3" type="email" name="email" id="email" placeholder="janis@gmail.com" id=""/>
                        </div>
                        <div className="input-group flex flex-column">
                            <label htmlFor="sender_testimonial">Atsauksme</label>
                            <textarea className="mb3 pv2 ph3" name="testimonial" id="" rows="5" id="sender_testimonial" placeholder="..."></textarea>
                        </div>

                        {/*<input className="dn" type="text" name="honey" value="" /> */}
                        <input className="dn" type="text" name="ajax" value="true" hidden />

                        <div className="input-group">
                            <button onClick={(e)=>{this.sendMail(e)}} type="submit" className={"fr button button--blue cp br-10 pv3 ph4 relative dib"+(this.formStatus==0?' o-20 pen':'')} to="/projekti/#kontakti">Iesniegt</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }

}

export default SubmitTestimonialBCK