import React from "react";
import Header from "../typography/header"
import "./testimonials.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'
import Img from "gatsby-image"
import FbIcon from "../../../static/icons/fb-icon.svg"
import GoogleIcon from "../../../static/icons/google-icon.svg"

import Reviews from '../../../data/testimonials.json'

library.add(fab)


class SubmitTestimonial extends React.Component {
    constructor(props) {
        super(props);
        this.formStatus = -1; // -1 unsent | 500 error | 200 okay | 0 waiting

        this.visibleReviewId = 0;
        this.reviewSize = Reviews.length;
        // setInterval(this.rotateReviews, 4000);
    }

    sendMail = (e) => {
        this.formStatus = 0;
        this.forceUpdate();
        
        e.preventDefault();

        let proxyUrl = 'https://cors-anywhere.herokuapp.com/';
        let targetUrl = 'https://chalete.lv/es/index.php';

        let formData = new FormData(document.getElementById("testimonial_form"));
        
        fetch(proxyUrl + targetUrl, {
        method: "POST",
        body: formData
        })
        .then(res => {
            return res.text();
        })
        .then(data => {
            this.formStatus = data;


            this.forceUpdate();

            if(this.formStatus == '500') {
                setTimeout(() =>{
                    this.formStatus = -1;
                    this.forceUpdate();
                },5000);
            }
        });
    }

    rotateReviews = () => {
        let allReviews = document.querySelectorAll(".review-item");

        for(var j = 0, length2 = allReviews.length; j < length2; j++){

            if(allReviews[j]!=undefined) {
                allReviews[j].classList.add("review-item--hidden");
            }
        }


        this.visibleReviewId+=1;
        if(this.visibleReviewId > this.reviewSize-1) {
            this.visibleReviewId = 0;
        }

        // console.log(allReviews[1]);
        if(allReviews[this.visibleReviewId]!=undefined) {
            allReviews[this.visibleReviewId].classList.remove("review-item--hidden");
        }
    }

    render() {
     
        if(this.props.ru) {
            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Оставьте нам свой отзыв</Header>
                        <p className="db pr5-l">
                            Обратная связь важна для любого бизнеса, потому что это один из способов, которыми мы можем развивать и улучшать качество наших услуг. Именно поэтому - если вы чем-то не удовлетворены или, наоборот, удивлены и восхищены, поделитесь!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                 <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center pt6-l">
                    <div className="flex flex-column ph5-l pt5">                  
                        
                        <div className="flex flex-row review-opt mb3 mb4-l">
                            <div className="w-20">
                                <img className="review-icon mr4 mb0" src={FbIcon}/>
                            </div>
                            <div className="w-80">
                                <a href="https://www.facebook.com/lmgroupbuve/reviews/" target="_blank" className="fb-btn ph2 pv3">Oбзор на Facebook</a>
                            </div>
                        </div>
                        
                        <div className="flex flex-row review-opt mb3 mb4-l">
                            <div className="w-20"></div>
                            <div className="w-80">
                                <span className="flex justify-center review-sep">ИЛИ</span>
                            </div>
                        </div>
                        
                        <div className="flex flex-row review-opt">
                            <div className="w-20">
                                <img className="review-icon mr4 mb0" src={GoogleIcon}/>
                            </div>
                            <div className="w-80">
                                <a href="https://g.co/kgs/HTmveU" target="_blank" className="google-btn ph2 pv3">Oбзор на Google</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                </div>
            )
        }

        if(this.props.en) {
            return (
                <div className="flex flex-column flex-row-l">
                    <div className="w-100 w-50-l">
                        <Header color="yellow" showLine>Leave your feedback</Header>
                        <p className="db pr5-l">
                            Feedback is important for any business because it is one of the ways we can develop and improve the quality of our services. That is why - if you are not satisfied with something or, on the contrary, surprised and delighted, share it!
                        </p>

                        {
                            Reviews.map((item, index) => {

                                return (
                                <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                    <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                        <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                    </a>

                                    <p className="mb0 pa3 grow pen">
                                        <i className="o-80">
                                        {item.review}
                                        </i> <br/>
                                    </p>
                                </div> 
                                )   
                            })
                        }
                    </div>
                    <div className="w-100 w-50-l items-center pt6-l">
                    <div className="flex flex-column ph5-l pt5">                  
                        
                        <div className="flex flex-row review-opt mb3 mb4-l">
                            <div className="w-20">
                                <img className="review-icon mr4 mb0" src={FbIcon}/>
                            </div>
                            <div className="w-80">
                                <a href="https://www.facebook.com/lmgroupbuve/reviews/" target="_blank" className="fb-btn ph2 pv3">Review on Facebook</a>
                            </div>
                        </div>
                        
                        <div className="flex flex-row review-opt mb3 mb4-l">
                            <div className="w-20"></div>
                            <div className="w-80">
                                <span className="flex justify-center review-sep">OR</span>
                            </div>
                        </div>
                        
                        <div className="flex flex-row review-opt">
                            <div className="w-20">
                                <img className="review-icon mr4 mb0" src={GoogleIcon}/>
                            </div>
                            <div className="w-80">
                                <a href="https://g.co/kgs/HTmveU" target="_blank" className="google-btn ph2 pv3">Review on Google</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
                </div>
            )
        }

        return (
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-50-l">
                    <Header color="yellow" showLine>Atstāj mums savu atsauksmi</Header>
                    <p className="db pr5-l">
                        Jebkuram uzņēmumam atsauksmes ir svarīgas, jo tas ir viens no veidiem, kā varam attīstīties un uzlabot pakalpojuma kvalitāti. Tieši tāpēc novērtēsim tavu viedokli par mūsu sadarbību, paldies!
                    </p>

                    {
                        Reviews.map((item, index) => {

                            return (
                             <div className={"pa1 pa3-l mb4 mb0-l testimonial-slider mt4 relative lightgray-border w-100 w-80-l br2 flex flex-column flex-row-l "+(index==0?"review-item":"review-item review-item--hidden")}>
                                <a href="https://www.facebook.com/lmgroupbuve/reviews" className="pt3 pt0-l ph2 o-80 flex justify-center items-center mb0 f1" title="Skatīt atsauksmes">
                                    <FontAwesomeIcon icon={['fab','facebook-square']}/>
                                </a>

                                <p className="mb0 pa3 grow pen">
                                    <i className="o-80">
                                    {item.review}
                                    </i> <br/>
                                </p>
                            </div> 
                            )   
                        })
                    }
                </div>
                <div className="w-100 w-50-l items-center pt6-l">
                    <div className="flex flex-column ph5-l pt5">                  
                        
                        <div className="flex flex-row review-opt mb3 mb4-l">
                            <div className="w-20">
                                <img className="review-icon mr4 mb0" src={FbIcon}/>
                            </div>
                            <div className="w-80">
                                <a href="https://www.facebook.com/lmgroupbuve/reviews/" target="_blank" className="fb-btn ph2 pv3">Atstāj atsauksmi Facebook</a>
                            </div>
                        </div>
                        
                        <div className="flex flex-row review-opt mb3 mb4-l">
                            <div className="w-20"></div>
                            <div className="w-80">
                                <span className="flex justify-center review-sep">VAI</span>
                            </div>
                        </div>
                        
                        <div className="flex flex-row review-opt">
                            <div className="w-20">
                                <img className="review-icon mr4 mb0" src={GoogleIcon}/>
                            </div>
                            <div className="w-80">
                                <a href="https://g.co/kgs/HTmveU" target="_blank" className="google-btn ph2 pv3">Atstāj atsauksmi Google</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        )
    }

}

export default SubmitTestimonial