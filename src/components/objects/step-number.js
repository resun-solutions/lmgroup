import React from "react"

const StepNumber = ({heading, children, number}) => {
  return (
    <li className="flex flex-column flex-row-l mt3 items-center items-start-l mb4 mb5-ns mb6-l">
        <span className="step-number z-1 2-100 tc w-30-l ms-bold yellow o-30-ns mt0 o-60">{number}</span>
        <div className="w-100 w-70-l">
            <h3 className="sec-font--bold tc tl-l">{heading}</h3>
            <p className="tj tl-l">
                {children}
            </p>
        </div>
    </li>
  )
}

export default StepNumber