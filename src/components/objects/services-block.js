import React from "react"

// import "./mini-navbar.scss"


const ServicesBlock = ({heading, children, icon}) => {
  return (
    <div className="flex flex-column mb4 mb5-ns w-100 w-40-ns">
        <div className="w-100 w-40-ns flex justify-center justify-start-ns">
            <img alt="Pakalpojuma ilustrācija" src={'/icons/'+icon} />
        </div>
        <div>
            <h2 className="ms-bold tc tl-ns">{heading}</h2>
            <p className="tj tl-ns">
                {children}
            </p>
        </div>
    </div>
  )
}

export default ServicesBlock