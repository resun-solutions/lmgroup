import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import "./mini-navbar.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
library.add(fas)
library.add(fab)


const MiniNavbar = () => {
  return (
      <div id="mini-navbar" className="ph0 ph4-ns ph0-l white cf container relative absolute w-100">
          <div className="fl w-50">
            <a title="Sazināties pa tālruni!" href="tel:22342440"><FontAwesomeIcon icon={["fas","phone"]} /> 22 342 440</a>
            <a title="Sazināties caur e-pastu!" className="ml3" href="email:info@lmgroup.lv"><FontAwesomeIcon icon={["fas","envelope"]} /> info@lmgroup.lv</a>
          </div>
          <div className="fl w-50 flex justify-end">
          <a title="Facebook profils" className="ml3" href="https://www.facebook.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","facebook-square"]} /></a>
            <a title="Instagram profils" className="ml3" href="https://www.instagram.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","instagram"]} /></a>
          </div>
      </div>
  )
}

export default MiniNavbar