import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { Parallax } from 'react-parallax';

import "./header.scss"

const Header = ({heading, url}) => {
  return (
    <div id="page-header">
      <Parallax bgImage={require('../../images/'+url)} bgImageAlt="Attēls ar modernām ēkām" strength={400}>
        <div className="landing-tint half-height-container"></div>
        <div id="landing-header-subpages" className="half-height-container container flex flex-column items-center justify-center items-start-ns justify-end-ns absolute">
            <h1 className="ttu ms-black white mb0 mb4-ns mb5-l">{heading}</h1>
        </div>
      </Parallax>
    </div>
  )
}

export default Header