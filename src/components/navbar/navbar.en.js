import React from "react"
import "./navbar.scss"
import { Link } from "gatsby"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import EmailPopup from "../email-popup/email-popup"
import Logo from "../../images/gatsby-icon_cropped.png"
import StoreContext from "../../context/storeContext"
library.add(fas)


class Navbar extends React.Component {

  constructor(props) {
    super(props);
    this.navbarOpen = false;
  }

  toggleNavbar = () => {
    this.navbarOpen = !this.navbarOpen;
    this.forceUpdate();
  }

  render() {
  const transitionHex = "#000";
  return (
    <StoreContext.Consumer>
      {
        storeData => (
          <>
          <div id="navbar" className="cf pv2 ph0 ph4-ns ph0-l container absolute mt-5 navbar-static w-100 z2">
              
              <div className="navbar-brand w-50 flex items-center">
                <Link className="mr4 ms-bold" paintDrip hex={transitionHex} to="/en">
                  <img alt="LM GROUP BUVE LOGO" className="navbar-brand-logo" src={Logo} />  
                  LM GROUP BUVE
                </Link>

                <div className="language-switch relative dn-s">
                 <Link to="/" className="o-70">LV</Link> | <Link to="/ru/" className="o-70">RU</Link> |  <Link to="/"><b>EN</b></Link>
                </div>
              </div>

              <div className={this.navbarOpen? "navbar-content w-50 flex justify-end" : "navbar-content navbar-content--hidden w-50 flex justify-end"}>
                <Link onClick={this.toggleNavbar} className="nav-link f2 pv2 mb2 mb3-m mb0-l mr3-l ph2-ns flex dn-ns" to="/en">Home</Link>
                <Link onClick={this.toggleNavbar} activeClassName="nav-link-active" className="nav-link f2 pv2 mb2 mb3-m mb0-l mr3-l ph2-ns" to="/en/par-mums">About us</Link>
                <Link onClick={this.toggleNavbar} activeClassName="nav-link-active" className="nav-link f2 pv2 mb2 mb3-m mb0-l mr3-l ph2-ns" to="/en/pakalpojumi">Services</Link>
                <Link onClick={this.toggleNavbar} activeClassName="nav-link-active" className="nav-link f2 pv2 mb2 mb3-m mb0-l mr3-l ph2-ns" to="/en/projekti">Projects</Link>
                
                <div className="nav-link flex f2 f5-ns pv2 pv1-l mb2 mb3-m mb0-l mr3-l ph2-ns nav-link-dropdown cp" to="/en/3d">
                  Products
                  <div className="dropdown-wrapper">
                    <div className="dropdown flex flex-column">
                      <Link className="w-100" to="/en/veikals/gazbetona-bloki">Concrete blocks</Link>
                      <Link className="w-100" to="/en/veikals/parsedzes">Overlays</Link>
                      <Link className="w-100" to="/en/veikals/parsegums">TERIVA Cover</Link>
                    </div>
                  </div>
                </div>

                <div className="nav-link dn flex-m flex-l f2 f5-ns pv2 pv1-l mb2 mb3-m mb0-l mr3-l ph2-ns nav-link-dropdown cp" to="/en/3d">
                  3D Visualization
                  <div className="dropdown-wrapper">
                    <div className="dropdown flex flex-column">
                      <Link className="w-100" to="/en/3d">3D Visualization</Link>
                      <Link className="dn flex-l w-100" to="/en/mebeles">Furniture</Link>
                    </div>
                  </div>
                </div>

                <Link onClick={this.toggleNavbar} activeClassName="nav-link-active" className="nav-link dn-l f2 pv2 mb4 mb3-m mb0-l mr3-l ph2-ns" to="/en/mebeles">Furniture</Link>
                
                <button onClick={storeData.toggleEmailPopup}  className="nav-link dn flex-m flex-l nav-link-contacts cp f2 f5-ns pv2 mb4 mb0-ns       ph2-ns" >Contacts</button>

                <Link onClick={this.toggleNavbar} className="nav-link dn-l f2 pv2 mb2 mb3-m mb0-l mr3-l ph2-ns" to="/ru/">
                  <img style={{
                    maxWidth: '50px',
                    boxShadow: '0px 0px 8px rgba(0,0,0,0.3)'
                  }} src='/icons/flag-ru.svg' />
                </Link>
                <Link onClick={this.toggleNavbar} className="nav-link dn-l f2 pv2 mb2 mb3-m mb0-l mr3-l ph2-ns" to="/">
                  <img style={{
                    maxWidth: '50px',
                    boxShadow: '0px 0px 8px rgba(0,0,0,0.3)'
                  }} src='/icons/flag-lv.svg' />
                </Link>

              </div>

              <div className="mobile-buttons flex fg pv1">
                <div title="Navigācija" role="button" onClick={this.toggleNavbar} className="cp w-third tc f4 flex justify-center items-center" data-sal="zoom-in" data-sal-delay="150" data-sal-duration="600">
                  <FontAwesomeIcon icon={ this.navbarOpen ? ["fas","times"] : ["fas","bars"]} />
                </div>
                <a title="Zvanīt uz nummuru" href="tel:22342440" className="w-third tc f4 flex justify-center items-center" data-sal="zoom-in" data-sal-delay="200" data-sal-duration="600">
                  <div className="pill-wrapper br-pill button--call green-gradient white">
                    <FontAwesomeIcon icon={["fas","phone"]} />
                  </div>
                </a>
                <div title="Rakstīt e-pastu" role="button" onClick={storeData.toggleEmailPopup} className="w-third tc f4 flex justify-center items-center" data-sal="zoom-in" data-sal-delay="250" data-sal-duration="600">
                  <FontAwesomeIcon icon={["fas","envelope"]} />
                </div>
              </div>

          </div>
          <EmailPopup />
          </>
        )
      }

    </StoreContext.Consumer>
  )
}
}

export default Navbar