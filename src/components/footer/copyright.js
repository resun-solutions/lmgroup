import React from "react"

import "./footer.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'
import BackToTop from "./back-to-top"
import Logo from "../../images/brand_logo.png"
library.add(fab)


const Copyright = ({ru, en}) => {

  if(ru) {
    return (
        <footer className="pa4 tc relative mb5 mb0-ns">
          <img alt="LM GROUP BUVE logo" style={{maxWidth: '60px'}} className="mb3" src={Logo} />
          <div className="mb3">
              <a title="Facebook Profils" href="https://www.facebook.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","facebook-square"]} /></a>
              <a title="Instargam Profils" className="ml3" href="https://www.instagram.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","instagram"]} /></a>
          </div>
          <p className="mb0 font-sec">Все права защищены | LM GROUP BUVE {new Date().getFullYear()}</p>
          <BackToTop />
        </footer>
    )    
  }

  if(en) {
    return (
        <footer className="pa4 tc relative mb5 mb0-ns">
          <img alt="LM GROUP BUVE logo" style={{maxWidth: '60px'}} className="mb3" src={Logo} />
          <div className="mb3">
              <a title="Facebook Profils" href="https://www.facebook.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","facebook-square"]} /></a>
              <a title="Instargam Profils" className="ml3" href="https://www.instagram.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","instagram"]} /></a>
          </div>
          <p className="mb0 font-sec">All rights reserved | LM GROUP BUVE {new Date().getFullYear()}</p>
          <BackToTop />
        </footer>
    )    
  }

  return (
      <footer className="pa4 tc relative mb5 mb0-ns">
        <img alt="LM GROUP BUVE logo" style={{maxWidth: '60px'}} className="mb3" src={Logo} />
        <div className="mb3">
            <a title="Facebook Profils" href="https://www.facebook.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","facebook-square"]} /></a>
            <a title="Instargam Profils" className="ml3" href="https://www.instagram.com/lmgroupbuve/" target="_blank" rel="noopener noreferrer nofollow"><FontAwesomeIcon icon={["fab","instagram"]} /></a>
        </div>
        <p className="mb0 font-sec">Visas tiesības aizsargātas | LM GROUP BUVE {new Date().getFullYear()}</p>
        <BackToTop />
      </footer>
  )
}

export default Copyright