import React from "react"

import "./footer.scss"

import { Parallax } from "react-parallax"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'

import emailjs from '@emailjs/browser';
import{ init } from '@emailjs/browser';

library.add(fas)

class MailForm extends React.Component {
    
    constructor(props) {
        super(props);
        this.formStatus = -1;
        
        this.state = {
            nameErrorType: "none",
            phoneErrorType: "none",
            emailErrorType: "none",
            messageErrorType: "none",
            name: "",
            email: "",
            phone: "",
            message: ""
        }
        
        init("Ipnx3URqcDB0EFLHd");
    }
    
        
    verifyFields = (field, val) => {
        
        if(field == "name") {
            if(val.length > 3) {
                this.setState({nameErrorType: "none"});
                return true;
            } 
            
            this.setState({nameErrorType: "incorrectVal"});
            return false;
        }
        
        if(field == "message") {
            if(val.length > 5) {
                this.setState({messageErrorType: "none"});
                return true;
            } 

            this.setState({messageErrorType: "incorrectVal"});
            return false;
        }

        if(field == "phone") {

            if(val.length < 5 || val.length > 16) {
                this.setState({phoneErrorType: "incorrectVal"});
                return false;
            } 
            
            this.setState({phoneErrorType: "none"});
            return true;
        }

        if(field == "email") {
            if(val.length < 6 || !val.includes("@")) {
                this.setState({emailErrorType: "incorrectVal"});
                return false;
            } 
            
            this.setState({emailErrorType: "none"});
            return true;
        }
    
    }
    
    emailJSSend = (templateId, variables) => {
        emailjs.send( 'service_v222it8', templateId, variables ).then(res => {
            alert("Ziņa nosūtīta veiksmīgi!");
            document.getElementById("email-footer-form").reset();
        })
        .catch(err => {
            alert("Ziņu neizdevās nosūtīt. Pārliecinies, ka visi lauki aizpildīti.");
            console.error('Oh well, you failed. Here some thoughts on the error that occured:', err);
        })
    }

    // Called right when button is pressed
    sendMail = (e) => {
        
        e.preventDefault();
        
        this.formStatus = 0;
        this.forceUpdate();
        
        let senderName = document.getElementById("sender_name2").value;
        let senderEmail = document.getElementById("sender_email2").value;
        let senderPhone = document.getElementById("sender_phone2").value;
        let senderMessage = document.getElementById("sender_message2").value;
        
        this.state.name = senderName;
        this.state.email = senderEmail;
        this.state.message = senderMessage;
        this.state.phone = senderPhone;
        
        this.verifyFields("name", senderName);
        this.verifyFields("message", senderMessage);
        this.verifyFields("phone", senderPhone);
        this.verifyFields("email", senderEmail);
        
        
        setTimeout(()=>{
            if(this.state.nameErrorType=="none"
            && this.state.phoneErrorType=="none"
            && this.state.emailErrorType=="none"
            && this.state.messageErrorType=="none") {
                this.emailJSSend("template_hfre0dd",{from_name: senderName, reply_to: senderEmail, message: senderMessage, phone: senderPhone});
            }
        },500);
    }
    
    render() {
        if (this.props.ru) {
            return (
                <Parallax bgImage={require('../../images/contacts-background.jpg')} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={400}>
                    <div id="kontakti" className="email-form-wrapper pv6 white dn db-l">
                        <div className="container">
                            <div className="flex">
                                <div className="w-100 w-60-m flex flex-column justify-center pv5 pvo-m">
                                    <h2 className="ms-bold">У вас есть вопрос?</h2>
                                    <p>
                                        Мы ответим в рабочие дни с <span className="yellow">07:00</span> до <span className="yellow">19:00</span> - в рабочее время
                                    </p>
                                    <div className="mt4 ">


                                    <span className="sec-font--bold f6">Matīss Legzdiņš <span className="sec-font">(Член правления)</span></span> <br/>
                                    <div className="">
                                        <a href="tel:22342440">+371 22 342 440</a><br/>
                                        <a>info@lmgroup.lv</a>
                                    </div>
                                    <hr className="w-30 mv3 o-40" style={{backgroundColor: 'white'}}/>
                                    <span className="sec-font--bold f6">Бухгалтер</span> <br/>
                                    <div className="">
                                        <a href="tel:22342440">+371 26 313 133</a><br/>
                                        <a>gramatvediba@lmgroup.lv</a>
                                    </div>

                                    </div>
                                </div>
                                <div className="w-100 w-40-m pb4 pb0-m">
                                    <form id="email-footer-form" className="email-form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                                        <div className="input-group flex flex-column">
                                            <label className="mb2" for="sender_name2">Ваше имя <span className="yellow">*</span></label>
                                            <input className="input-reset mb3 pv2 ph3" type="text" name="name" id="sender_name2" placeholder="Jānis Bērziņš" />
                                        </div>
                                        <div className="input-group flex flex-column">
                                        <label className="mb2" for="sender_email2">Ваш электронный адрес <span className="yellow">*</span></label>
                                            <input className="input-reset mb3 pv2 ph3" type="email" name="email" id="sender_email2" placeholder="janis@inbox.lv" />
                                        </div>
                                        <div className="input-group flex flex-column">
                                        <label className="mb2" for="sender_phone2">Ваш номер телефона</label>
                                            <input className="input-reset mb3 pv2 ph3" type="phone" name="phone" id="sender_phone2" placeholder="12 345 678" />
                                        </div>
                                        <div className="input-group flex flex-column">
                                            <label className="mb2" for="sender_message2">Письмо <span className="yellow">*</span></label>
                                            <textarea className="input-reset mb3 pv2 ph3 rn" rows="4" name="message2" id="sender_message2" placeholder="..."></textarea>
                                        </div>
                                        {/* <input className="dn" type="text" name="token" value="hhnn44a" /> */}
                                        <button onClick={(e) => {this.sendMail(e)}} className="button button--yellow cp black br-2 pv3 ph4 relative fr mt3" type="submit">Отправить</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </Parallax>
            )
        }
        else if (this.props.en) {
            return (
                <Parallax bgImage={require('../../images/contacts-background.jpg')} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={400}>
                    <div id="kontakti" className="email-form-wrapper pv6 white dn db-l">
                        <div className="container">
                            <div className="flex">
                                <div className="w-100 w-60-m flex flex-column justify-center pv5 pvo-m">
                                    <h2 className="ms-bold">Do you have any questions</h2>
                                    <p>
                                        Get a reply from <span className="yellow">07:00</span> to <span className="yellow">19:00</span> all work days
                                    </p>
                                    <div className="mt4 ">


                                    <span className="sec-font--bold f6">Matīss Legzdiņš <span className="sec-font">(Direktor)</span></span> <br/>
                                    <div className="">
                                        <a href="tel:22342440">+371 22 342 440</a><br/>
                                        <a>info@lmgroup.lv</a>
                                    </div>
                                    <hr className="w-30 mv3 o-40" style={{backgroundColor: 'white'}}/>
                                    <span className="sec-font--bold f6">Accountant</span> <br/>
                                    <div className="">
                                        <a href="tel:22342440">+371 26 313 133</a><br/>
                                        <a>gramatvediba@lmgroup.lv</a>
                                    </div>

                                    </div>
                                </div>
                                <div className="w-100 w-40-m pb4 pb0-m">
                                    <form id="email-footer-form" className="email-form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                                        <div className="input-group flex flex-column">
                                            <label className="mb2" for="sender_name2">Your name <span className="yellow">*</span></label>
                                            <input className="input-reset mb3 pv2 ph3" type="text" name="name" id="sender_name2" placeholder="Jānis Bērziņš" />
                                        </div>
                                        <div className="input-group flex flex-column">
                                        <label className="mb2" for="sender_email2">Your email <span className="yellow">*</span></label>
                                            <input className="input-reset mb3 pv2 ph3" type="email" name="email" id="sender_email2" placeholder="janis@inbox.lv" />
                                        </div>
                                        <div className="input-group flex flex-column">
                                        <label className="mb2" for="sender_phone2">Your phone number</label>
                                            <input className="input-reset mb3 pv2 ph3" type="phone" name="phone" id="sender_phone2" placeholder="12 345 678" />
                                        </div>
                                        <div className="input-group flex flex-column">
                                            <label className="mb2" for="sender_message2">Content <span className="yellow">*</span></label>
                                            <textarea className="input-reset mb3 pv2 ph3 rn" rows="4" name="message" id="sender_message2" placeholder="..."></textarea>
                                        </div>
                                        {/* <input className="dn" type="text" name="token" value="hhnn44a" /> */}
                                        <button onClick={(e) => {this.sendMail(e)}} className="button button--yellow cp black br-2 pv3 ph4 relative fr mt3" type="submit">Send</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </Parallax>
            )
        }else {
            return (
                <Parallax bgImage={require('../../images/contacts-background.jpg')} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={400}>
                <div id="kontakti" className="email-form-wrapper pv6 white dn db-l">
                    <div className="container">
                        <div className="flex">
                            <div className="w-100 w-60-m flex flex-column justify-center pv5 pvo-m">
                                <h2 className="ms-bold">Vai Tev ir kāds jautājums?</h2>
                                <p>
                                    Atbildēsim darba dienās no <span className="yellow">07:00</span> līdz <span className="yellow">19:00</span> - stundas laikā
                                </p>
                                <div className="mt4 ">
          
          
                                  <span className="sec-font--bold f6">Matīss Legzdiņš <span className="sec-font">(Valdes loceklis)</span></span> <br/>
                                  <div className="">
                                      <a href="tel:22342440">+371 22 342 440</a><br/>
                                      <a>info@lmgroup.lv</a>
                                  </div>
                                  <hr className="w-30 mv3 o-40" style={{backgroundColor: 'white'}}/>
                                  <span className="sec-font--bold f6">Grāmatvede</span> <br/>
                                  <div className="">
                                      <a href="tel:22342440">+371 26 313 133</a><br/>
                                      <a>gramatvediba@lmgroup.lv</a>
                                  </div>
          
                                </div>
                            </div>
                            <div className="w-100 w-40-m pb4 pb0-m">
                                <form id="email-footer-form" className="email-form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                                    <div className="input-group flex flex-column">
                                        <label className="mb2" for="sender_name2">Tavs vārds <span className="yellow">*</span></label>
                                        <input className="input-reset mb3 pv2 ph3" type="text" name="name" id="sender_name2" placeholder="Jānis Bērziņš" />
                                    </div>
                                    <div className="input-group flex flex-column">
                                    <label className="mb2" for="sender_email2">Tavs e-pasts <span className="yellow">*</span></label>
                                        <input className="input-reset mb3 pv2 ph3" type="email" name="email" id="sender_email2" placeholder="janis@inbox.lv" />
                                    </div>
                                    <div className="input-group flex flex-column">
                                    <label className="mb2" for="sender_phone2">Tavs tel. nr.</label>
                                        <input className="input-reset mb3 pv2 ph3" type="phone" name="phone" id="sender_phone2" placeholder="12 345 678" />
                                    </div>
                                    <div className="input-group flex flex-column">
                                          <label className="mb2" for="sender_message2">Ziņa <span className="yellow">*</span></label>
                                          <textarea className="input-reset mb3 pv2 ph3 rn" rows="4" name="message" id="sender_message2" placeholder="..."></textarea>
                                    </div>
                                    {/* <input className="dn" type="text" name="token" value="hhnn44a" /> */}
                                    <button onClick={(e) => {this.sendMail(e)}} className="button button--yellow cp black br-2 pv3 ph4 relative fr mt3">Nosūtīt</button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                </Parallax>
            )
        }
    }

}

export default MailForm