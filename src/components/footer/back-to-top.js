import React from "react"
import { Link } from "gatsby"
import "./footer.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)

const BackToTop = () => {

  const scrollToTop = () => {
    window.scrollTo(0, 0);
  }

  return (
      <button title="Uz augšu" onClick={scrollToTop} id="back-to-top" className="dn flex-ns absolute t50 r0 mr5 br-pill cp items-center justify-center">
          <FontAwesomeIcon icon={['fas', 'chevron-up']} />
      </button>
  )
}

export default BackToTop