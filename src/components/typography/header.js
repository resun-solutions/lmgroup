import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import "./header.scss"

const Header = ({children, color, showLine, center, level}) => {

    if(showLine) {

        if(center) {
            return (
                <div className="flex flex-column items-center pt5 pb4 pt6-m pb5-m pv6-l">
                    <div className={"mb3 mb4-l header-line header-line--"+color}></div>
                    <h2 className={"header-title ttu ms-bold tc w-80 w-100-ns"}>{children}</h2>
                </div>
            )
        }else {
            return (
                <div className="pt6-ns pt5">
                    <div className={"mb3 mb4-ns header-line header-line--"+color}></div>
                    <h2 className={"header-title ttu ms-bold"}>{children}</h2>
                </div>
            )
        }

    }else {
        return <h2 className={"header-title ttu tc"}>{children}</h2>
    }

}

export default Header
