import React from "react"

import "./email-popup.scss"
import "../overlay/style.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'
import Overlay from "../overlay/overlay"
import StoreContext from "../../context/storeContext"

import emailjs from '@emailjs/browser';
import{ init } from '@emailjs/browser';



library.add(fab)


class EmailPopup extends React.Component {
    
    constructor(props) {
        super(props);
        this.visible = true;
        this.overlayVisible = true;
        this.formStatus = -1;
        
        this.state = {
            nameErrorType: "none",
            phoneErrorType: "none",
            emailErrorType: "none",
            messageErrorType: "none",
            name: "",
            email: "",
            phone: "",
            message: ""
        }
        
        init("Ipnx3URqcDB0EFLHd");
    }
    
    verifyFields = (field, val) => {
        
        if(field == "name") {
            if(val.length > 3) {
                this.setState({nameErrorType: "none"});
                return true;
            } 
            
            this.setState({nameErrorType: "incorrectVal"});
            return false;
        }
        
        if(field == "message") {
            if(val.length > 5) {
                this.setState({messageErrorType: "none"});
                return true;
            } 

            this.setState({messageErrorType: "incorrectVal"});
            return false;
        }

        if(field == "phone") {

            if(val.length < 5 || val.length > 16) {
                this.setState({phoneErrorType: "incorrectVal"});
                return false;
            } 
            
            this.setState({phoneErrorType: "none"});
            return true;
        }

        if(field == "email") {
            if(val.length < 6 || !val.includes("@")) {
                this.setState({emailErrorType: "incorrectVal"});
                return false;
            } 
            
            this.setState({emailErrorType: "none"});
            return true;
        }
    
    }
    
    emailJSSend = (templateId, variables) => {

        emailjs.send( 'service_v222it8', templateId, variables ).then(res => {
            alert("Ziņa nosūtīta veiksmīgi!");
            this.toggle();
            document.getElementById("email-popup-form").reset();
        })
        .catch(err => {
            alert("Ziņu neizdevās nosūtīt. Pārliecinies, ka visi lauki aizpildīti.");
            console.error('Oh well, you failed. Here some thoughts on the error that occured:', err);
        })
    }

    // Called right when button is pressed
    sendMail = (e) => {
        
        e.preventDefault();

        this.formStatus = 0;
        this.forceUpdate();

        let senderName = document.getElementById("sender_name").value;
        let senderEmail = document.getElementById("sender_email").value;
        let senderPhone = document.getElementById("sender_phone").value;
        let senderMessage = document.getElementById("sender_message").value;
        
        this.state.name = senderName;
        this.state.email = senderEmail;
        this.state.message = senderMessage;
        this.state.phone = senderPhone;
        
        this.verifyFields("name", senderName);
        this.verifyFields("message", senderMessage);
        this.verifyFields("phone", senderPhone);
        this.verifyFields("email", senderEmail);
        
        
        setTimeout(()=>{
            if(this.state.nameErrorType=="none"
            && this.state.phoneErrorType=="none"
            && this.state.emailErrorType=="none"
            && this.state.messageErrorType=="none") {
                this.emailJSSend("template_hfre0dd",{from_name: senderName, reply_to: senderEmail, message: senderMessage, phone: senderPhone});
            }
        },500);
        
        
        // fetch(proxyUrl + targetUrl, {
        // method: "POST",
        // body: formData
        // })
        // .then(res => {
        //     return res.text();
        // })
        // .then(data => {
        //     this.formStatus = data;

        //     this.forceUpdate();

        //     if(this.formStatus == '500') {
        //         alert("Neizdevās nosūtīt!");
        //     }else {
        //         document.getElementById("email-popup-form").reset();
        //         this.toggle();
        //     }
        // });
    }

    toggle = () => {
        
        this.context.toggleEmailPopup();
        
    }

    render() {
        return (
            <StoreContext.Consumer>
                {
                    storeData => {

                        if(storeData.isRussian) {
                            return (
                                <>
                                    <div id="email-popup" className={(storeData.emailPopupOpen ? "email-popup--fly-in " : "email-popup--hidden ")+"fixed b0 r0 w-100 w-80-ns w-40-l flex flex-column"}>
                                        <div className="form-wrapper fg pa3 pa4-l">
                                            <form id="email-popup-form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_name">Ваше имя <span className="green">*</span></label>
                                                    <input className="input-reset mb3 pv2 ph3" name="name" id="sender_name" type="text" placeholder="Jānis Bērziņš" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_email">Ваш электронный адрес <span className="green">*</span></label>
                                                    <input className="input-reset mb3 pv2 ph3" name="email" id="sender_email" type="email" placeholder="janis@inbox.lv" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_phone">Твой телефон нр.</label>
                                                    <input className="input-reset mb3 pv2 ph3" type="phone" name="phone" id="sender_phone" placeholder="12 345 678" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_message">Письмо <span className="green">*</span></label>
                                                    <textarea className="input-reset mb3 pv2 ph3 rn" name="message" id="sender_message" rows="4" placeholder="..."></textarea>
                                                </div>
                                                <p className="o-70 f6 tc">Отправляя это электронное письмо, вы соглашаетесь с условиями использования сайта</p>
                                                <div className="popup-navigation flex absolute l0 b0 w-100">
                                                    <button type="button" onClick={storeData.toggleEmailPopup} className="w-100 pv3 sec-font cp">Закрывать</button>
                                                    <button type="submit" onClick={(e) => {this.sendMail(e)}} className={"w-100 pv3 sec-font send cp"+(this.formStatus==0?' o-20 pen':'')}>Отправить</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div className={"pen overlay-component fixed t0 l0 w-100 h-100 state-"+storeData.emailPopupOpen}>a</div>
                                </>
                            )
                        }
                        if(storeData.isEnglish) {
                            return (
                                <>
                                    <div id="email-popup" className={(storeData.emailPopupOpen ? "email-popup--fly-in " : "email-popup--hidden ")+"fixed b0 r0 w-100 w-80-ns w-40-l flex flex-column"}>
                                        <div className="form-wrapper fg pa3 pa4-l">
                                            <form id="email-popup-form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_name">Your name <span className="green">*</span></label>
                                                    <input className="input-reset mb3 pv2 ph3" name="name" id="sender_name" type="text" placeholder="Jānis Bērziņš" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_email">Your email <span className="green">*</span></label>
                                                    <input className="input-reset mb3 pv2 ph3" name="email" id="sender_email" type="email" placeholder="janis@inbox.lv" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_phone">Your phone number</label>
                                                    <input className="input-reset mb3 pv2 ph3" type="phone" name="phone" id="sender_phone" placeholder="12 345 678" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_message">Content <span className="green">*</span></label>
                                                    <textarea className="input-reset mb3 pv2 ph3 rn" name="message" id="sender_message" rows="4" placeholder="..."></textarea>
                                                </div>
                                                <p className="o-70 f6 tc">By sending this email, you agree to the terms of use of the site</p>
                                                <div className="popup-navigation flex absolute l0 b0 w-100">
                                                    <button type="button" onClick={storeData.toggleEmailPopup} className="w-100 pv3 sec-font cp">Close</button>
                                                    <button type="submit" onClick={(e) => {this.sendMail(e)}} className={"w-100 pv3 sec-font send cp"+(this.formStatus==0?' o-20 pen':'')}>Send</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div className={"pen overlay-component fixed t0 l0 w-100 h-100 state-"+storeData.emailPopupOpen}>a</div>
                                </>
                            )
                        }
                        else {
                            return (
                                <>
                                    <div id="email-popup" className={(storeData.emailPopupOpen ? "email-popup--fly-in " : "email-popup--hidden ")+"fixed b0 r0 w-100 w-80-ns w-40-l flex flex-column"}>
                                        <div className="form-wrapper fg pa3 pa4-l">
                                            <form id="email-popup-form" action="https://chalete.lv/es/index.php" method="POST" target="_blank">
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_name">Tavs vārds <span className="green">*</span></label>
                                                    <input className="input-reset mb3 pv2 ph3" name="name" id="sender_name" type="text" placeholder="Jānis Bērziņš" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_email">Tavs e-pasts <span className="green">*</span></label>
                                                    <input className="input-reset mb3 pv2 ph3" name="email" id="sender_email" type="email" placeholder="janis@inbox.lv" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_phone">Tavs tel. nr.</label>
                                                    <input className="input-reset mb3 pv2 ph3" type="phone" name="phone" id="sender_phone" placeholder="12 345 678" />
                                                </div>
                                                <div className="input-group flex flex-column">
                                                    <label className="mb2" htmlFor="sender_message">Ziņa <span className="green">*</span></label>
                                                    <textarea className="input-reset mb3 pv2 ph3 rn" name="message" id="sender_message" rows="4" placeholder="..."></textarea>
                                                </div>
                                                <p className="o-70 f6 tc">Nosūtot šo e-pastu, jūs piekrītat mājaslapas lietošanas noteikumiem.</p>
                                                <div className="popup-navigation flex absolute l0 b0 w-100">
                                                    <button type="button" onClick={storeData.toggleEmailPopup} className="w-100 pv3 sec-font cp">Aizvērt</button>
                                                    <button type="submit" onClick={(e) => {this.sendMail(e)}} className={"w-100 pv3 sec-font send cp"+(this.formStatus==0?' o-20 pen':'')}>Sūtīt</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div className={"pen overlay-component fixed t0 l0 w-100 h-100 state-"+storeData.emailPopupOpen}>a</div>
                                </>
                            )
                        }

                    }
                }
            </StoreContext.Consumer>
        )
    }
}

EmailPopup.contextType = StoreContext;
export default EmailPopup