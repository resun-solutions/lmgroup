import React from "react"
import { Link, graphql } from "gatsby"
import "./style.scss"
import { Parallax } from 'react-parallax';
import StoreContext from "../../context/storeContext"
import FloatingCart from "../store/floatingCart.js"
import ProjectJSON from "../../../data/projects.json"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import Img from "gatsby-image"
library.add(fas)

class Gallery extends React.Component {

  constructor(props) {
    super(props);

    this.currentImgId = 1;
  }

  updateImg = (newId) => {
    this.currentImgId = newId;
    this.forceUpdate();
  }

  render() {

    return (
        <StoreContext.Consumer>
          {
            storeData => {
              let tmpGalleryItemId = 0;
              let currentImgId = 1;

              if(storeData.galleryOpen) {
                tmpGalleryItemId = storeData.galleryItemId;

                return (
                <div className={"gallery-overlay gallery-open--"+storeData.galleryOpen}>

                <div className="gallery-wrapper relative br2">

                  <div className="main-pic-wrapper relative">
                    <img src={'/project_img/'+(1+storeData.galleryItemId)+'/'+this.currentImgId+'.jpg'} className="main-pic br1" />                  
                    <div className={this.props.projects[tmpGalleryItemId].node.gallerySize==1?'dn':'folder'}>
                      {
                        this.props.projects[tmpGalleryItemId].node.gallerySize.map((photo, index) => {
                         if(index>0) {
                              return (
                                <img onClick={() => { this.updateImg(1+index) }} className="folder-pic ma0 pa0 mh1 br1 cp" src={'/project_img/'+(1+storeData.galleryItemId)+'/'+(1+index)+'.jpg'} />
                                )
                          }   
                        })
                      }
                    </div>
                    
                    <button className="pointer" onClick={() => {storeData.toggleGallery(); this.currentImgId=1}}>
                    <FontAwesomeIcon icon={['fas','times']}/>
                    </button>

                  </div>
                </div>
                </div>
                )
                }
                return '';
              }



          }
        </StoreContext.Consumer>
      )
  }
}

export default Gallery
