import React from "react"
import { Link } from "gatsby"
import "./style.scss"
import { Parallax } from 'react-parallax';
import StoreContext from "../../context/storeContext"
import FloatingCart from "../store/floatingCart.js"
import ProductJSON from "../../../data/store_data/products.json"

class Calculator extends React.Component {

  constructor(props) {
    super(props);

    this.calcV = this.props.version;

    this.dx      = null;
    this.dy      = null;
    this.dz      = null;
    this.density = null;
    this.dzmax   = null;

    this.cost = 0;
    this.paletes = 0;
    this.m3 = 0;
    this.prodId = null;
    this.addedProduct = null;
  }

  calculatePrice = () => {

    const paletesTilpums = 1.8;
    const pilnaKrava     = 39.6;

    // Gazbetona bloki
    if(this.calcV == 0) {

      let dx           = document.getElementById("platums").value;
      let dy           = document.getElementById("augstums").value;
      let dz           = document.getElementById("garums").value;
      let density      = document.getElementById("blivums").value;
      let paletes      = document.getElementById("paletes").value;
      let m3           = document.getElementById("m3").value;
      let products     = ProductJSON[0].products;
      let matchingProd = null;

      products.forEach( (product, index) => {
        if (  product.dx == dx && product.dy == dy && product.dz == dz && product.density == density ) {
          matchingProd = product;
        }
      });

      if(matchingProd == null) {
        alert("Izvēlētā konfigurācija nav pieejama");
        return;
      }

      this.m3 = paletes*paletesTilpums;
      this.paletes = paletes;
      this.cost = paletes*matchingProd.cost;
      this.prodId = matchingProd.productId;
      this.addedProduct = matchingProd;

      this.forceUpdate();
    } else {

      let dx           = document.getElementById("platums").value;
      let dy           = document.getElementById("augstums").value;
      let dz           = document.getElementById("garums").value;
      let dxmax       = document.getElementById("garums_max").value;
      let paletes      = document.getElementById("paletes").value;
      let m3           = document.getElementById("m3").value;
      let products     = ProductJSON[1].products;
      let matchingProd = null;

      products.forEach( (product, index) => {
        if (  product.dx == dx && product.dy == dy && product.dz == dz ) {
          matchingProd = product;
          matchingProd.productId = index;
        }
      });

      console.log("PROD", matchingProd);

      if(matchingProd == null) {
        alert("Izvēlētā konfigurācija nav pieejama");
        return;
      }

      this.m3 = paletes*paletesTilpums;
      this.paletes = paletes;
      this.cost = paletes*matchingProd.cost;
      this.prodId = matchingProd.productId;
      this.addedProduct = matchingProd;

      this.forceUpdate();
    }

  }


  filterChanged = (whatChanged) => {

    if(this.props.version == '0') {

      this.dx      = document.getElementById("platums").value;
      this.density = document.getElementById("blivums").value;
      let products = ProductJSON[0].products;
      
      let matchingProducts = [];
      let dxOptions        = [];
      let densityOptions   = [];

      products.forEach((product, index) => {

        let pointsToMatch = 2;
        let points = 0;

        if(this.dx == "null" || this.dx == product.dx) points+=1;
        if(this.density == "null" || this.density == product.density) points+=1;

        if(points == pointsToMatch) {
          matchingProducts.push(product);

          if (!dxOptions.includes(product.dx)) {
            dxOptions.push(product.dx);
          }
          if (!densityOptions.includes(product.density)) {
            densityOptions.push(product.density);
          }
        }

      });

      let dxOptionsFields      = document.getElementById("platums").options;
      let densityOptionsFields = document.getElementById("blivums").options;

      Object.keys(dxOptionsFields).forEach((option, index) => {

        if(index != 0) {
          dxOptionsFields[option].classList.remove("option--unavailable");

          if( !dxOptions.includes(dxOptionsFields[option].value) ) {
            dxOptionsFields[option].classList.add("option--unavailable");
          }
        }     

      });

      Object.keys(densityOptionsFields).forEach((option, index) => {

        if(index != 0) {
          densityOptionsFields[option].classList.remove("option--unavailable");

          if( !densityOptions.includes(densityOptionsFields[option].value) ) {
            densityOptionsFields[option].classList.add("option--unavailable");
          }
        }     

      });

    } else {

      this.dx      = document.getElementById("platums").value;
      this.dy      = document.getElementById("augstums").value;
      this.dz      = document.getElementById("garums").value;
      let products = ProductJSON[1].products;
      
      let matchingProducts = [];

      let dxOptions        = [];
      let dyOptions        = [];
      let dzOptions        = [];

      products.forEach((product, index) => {

        let pointsToMatch = 3;
        let points = 0;

        if(this.dx    == "null" || this.dx == product.dx) points+=1;
        if(this.dy    == "null" || this.dy == product.dy) points+=1;
        if(this.dz    == "null" || this.dz == product.dz) points+=1;

        if(points == pointsToMatch) {

          matchingProducts.push(product);

          if (!dxOptions.includes(product.dx)) {
            dxOptions.push(product.dx);
          }
          if (!dyOptions.includes(product.dy)) {
            dyOptions.push(product.dy);
          }
          if (!dzOptions.includes(product.dz)) {
            dzOptions.push(product.dz);
          }
          
        }

      });


      let dxOptionsFields    = document.getElementById("platums").options;
      let dyOptionsFields    = document.getElementById("augstums").options;
      let dzOptionsFields    = document.getElementById("garums").options;

      Object.keys(dxOptionsFields).forEach((option, index) => {

        if(index != 0) {
          dxOptionsFields[option].classList.remove("option--unavailable");

          if( !dxOptions.includes(dxOptionsFields[option].value) ) {
            dxOptionsFields[option].classList.add("option--unavailable");
          }
        }     

      });

      Object.keys(dyOptionsFields).forEach((option, index) => {

        if(index != 0) {
          dyOptionsFields[option].classList.remove("option--unavailable");

          if( !dyOptions.includes(dyOptionsFields[option].value) ) {
            dyOptionsFields[option].classList.add("option--unavailable");
          }
        }     

      });

      Object.keys(dzOptionsFields).forEach((option, index) => {

        if(index != 0) {
          dzOptionsFields[option].classList.remove("option--unavailable");

          if( !dzOptions.includes(dzOptionsFields[option].value) ) {
            dzOptionsFields[option].classList.add("option--unavailable");
          }
        }     

      });


      console.log(matchingProducts);


      // if(matchingProducts.size == 1) {
        document.getElementById("garums_max").value = matchingProducts[0].dz - matchingProducts[0].dzmax;
      // }

    }

  }

  render () {

    if(this.props.version == '0') {
      return (
        <>
        <StoreContext.Consumer>
          {
            storeData => (
              <div id="calculator" className="pa4 pa5-m flex flex-column">
                
                <div className={"mb4 header-line header-line--yellow"}></div>

                <h3 className="mb4 mb5-l b">Izvēlies un pasūti jau šodien</h3>
                
                <div className="flex flex-wrap">

                  <div className="mb2 mb0-l w-100 w-50-m w-25-l pr4-l">
                    <label htmlFor="length" className="b pb2 db">Bloka platums</label>
                    <select onChange={() => { this.filterChanged("platums") }} id="platums" className="pa2 w-100"> 
                        <option value="null">Izvēlies platumu</option>
                        <option value="100">100</option>
                        <option value="150">150</option>
                        <option value="240">240</option>
                        <option value="200">200</option>
                        <option value="300">300</option>
                        <option value="375">375</option>
                        <option value="400">400</option>
                        <option value="480">480</option>
                    </select> 
                  </div>

                  <div className="mb2 mb0-l w-100 w-50-m w-25-l pr4-l">
                    <label htmlFor="augstums" className="b pb2 db">Bloka augstums</label>
                    <input type="text" disabled id="augstums" className="pa2 w-100 trans" value="250" />
                  </div>

                  <div className="mb2 mb0-l w-100 w-50-m w-25-l pr4-l">
                    <label htmlFor="garums" className="b pb2 db">Bloka garums</label>
                    <input type="text" disabled id="garums" className="pa2 w-100 trans" value="600" />
                  </div>

                  <div className="mb2 mb0-l w-100 w-50-m w-25-l">
                    <label htmlFor="blivums" className="b pb2 db">Bloka blīvums</label>
                    <select onChange={() => { this.filterChanged("platums") }} id="blivums" className="pa2 w-100"> 
                        <option value="null">Izvēlies blīvumu</option>
                        <option value="350">350</option>
                        <option value="375">375</option>
                        <option value="400">400</option>
                        <option value="500">500</option>
                    </select> 
                  </div>

                </div>
                
                <hr className="mv4 mv5-l"/>

                <div className="flex flex-wrap">
                  <div className="mb2 mb0-l w-100 w-50-m w-25-l pr4-l">
                    <label htmlFor="paletes" className="b pb2 db">Paletes</label>
                    <input onChange={() => {this.calculatePrice("paletes")}} type="number" pattern="[1-2000]*" id="paletes" min="1" className="pa2 w-100" placeholder="Palešu skaits" />
                  </div>

                  <div className="mb2 mb0-l w-100 w-50-m w-25-l pr4-l">
                    <label htmlFor="m3" className="b pb2 db">Pasūtījuma tilpums</label>
                    <input disabled type="text" pattern="[1-2000]*" id="m3" min="1" className="pa2 w-100" placeholder="m3" value={this.m3+" (m3)"}/>
                  </div>

                  <div className="mb2 mb0-l w-100 w-50-m w-25-l pr4-l">
                    <span htmlFor="cost" className="b pb2 db">Kopējā summa</span>
                    <input type="text" disabled name="cost" id="cost" min="1" className="pa2 w-100 trans" value={this.cost+" (€)"} />
                  </div>

                  <div className="w-100 w-50-m w-25-l">
                    <span htmlFor="cost2" className="b pb2 db">Kopējā summa ar PVN</span>
                    <input type="text" disabled name="cost2" id="cost_final" min="1" className="pa2 w-100" value={(this.cost+(this.cost*0.21))+" (€)"} />
                  </div>
                </div>
                <div className="mt4 mt5-l">
                <button onClick={() => { storeData.addToCart(this.prodId, this.addedProduct, this.paletes) }} className="pv3 w-100 w-auto-l ph4 br2 fr button--blue dib no-border cp t25">Pievienot grozam</button>
                </div>
              </div>
              
            )
          }
        </StoreContext.Consumer>

        <FloatingCart />
        </>
      )
    }else {
      return (
        <>
        <StoreContext.Consumer>
          {
            storeData => (
              <div id="calculator" className="pa4 pa5-m flex flex-column">
                
                <div className={"mb4 header-line header-line--yellow"}></div>

                <h3 className="mb4 mb5-l b">Izvēlies un pasūti jau šodien</h3>
                
                <div className="flex flex-wrap">

                  <div className="w-100 w-50-m w-25-l pr4-l mb2 mb0-l">
                    <label htmlFor="length" className="b pb2 db">Pārsedzes platums</label>
                    <select onChange={() => { this.filterChanged("platums") }} id="platums" className="pa2 w-100"> 
                        <option value="null">Izvēlies platumu</option>
                        <option value="100">100</option>
                        <option value="115">115</option>
                        <option value="150">150</option>
                        <option value="200">200</option>
                        <option value="250/240">250/240</option>
                        <option value="300">300</option>
                        <option value="365/375">365/375</option>
                        <option value="400">400</option>
                        <option value="480">480</option>
                    </select> 
                  </div>

                  <div className="w-100 w-50-m w-25-l pr4-l mb2 mb0-l">
                    <label htmlFor="augstums" className="b pb2 db">Pārsedzes augstums</label>
                    <select onChange={() => { this.filterChanged("platums") }} id="augstums" className="pa2 w-100"> 
                        <option value="null">Izvēlies augstumu</option>
                        <option value="200">200</option>
                        <option value="250">250</option>
                        <option value="300">300</option>
                    </select> 
                  </div>

                  <div className="w-100 w-50-m w-25-l pr4-l mb2 mb0-l">
                    <label htmlFor="garums" className="b pb2 db">Pārsedzes garums</label>
                    <select onChange={() => { this.filterChanged("platums") }} id="garums" className="pa2 w-100">
                        <option value="null">Izvēlies garumu</option> 
                        <option value="900">900</option>
                        <option value="1000">1000</option>
                        <option value="1200">1200</option>
                        <option value="1300">1300</option>
                        <option value="1400">1400</option>
                        <option value="1500">1500</option>
                        <option value="1600">1600</option>
                        <option value="1700">1700</option>
                        <option value="1800">1800</option>
                        <option value="2000">2000</option>
                        <option value="2100">2100</option>
                        <option value="2200">2200</option>
                        <option value="2400">2400</option>
                        <option value="2500">2500</option>
                        <option value="2700">2700</option>
                        <option value="3000">3000</option>
                        <option value="3300">3300</option>
                        <option value="3500">3500</option>
                        <option value="3800">3800</option>
                        <option value="4000">4000</option>
                        <option value="4200">4200</option>
                        <option value="4500">4500</option>
                        <option value="5000">5000</option>
                        <option value="5500">5500</option>
                        <option value="6000">6000</option>
                    </select> 
                  </div>

                  <div className="w-100 w-50-m w-25-l">
                    <label htmlFor="garums_max" className="b pb2 db">Maksimālais ailes garums</label>
                    <input type="text" disabled name="cost" id="garums_max" min="1" className="pa2 w-100 trans" value="Maksimālais ailes garums" />
                  </div>

                </div>
                
                <hr className="mv4 mv5-l"/>

                <div className="flex flex-wrap">
                  <div className="w-100 w-50-m w-25-l pr4-l mb2 mb0-l">
                    <label htmlFor="paletes" className="b pb2 db">Paletes</label>
                    <input onChange={() => {this.calculatePrice("paletes")}} type="number" pattern="[1-2000]*" id="paletes" min="1" className="pa2 w-100" placeholder="Palešu skaits" />
                  </div>

                  <div className="w-100 w-50-m w-25-l pr4-l mb2 mb0-l">
                    <label htmlFor="m3" className="b pb2 db">Pasūtījuma tilpums</label>
                    <input disabled type="text" pattern="[1-2000]*" id="m3" min="1" className="pa2 w-100" placeholder="m3" value={this.m3+" (m3)"}/>
                  </div>

                  <div className="w-100 w-50-m w-25-l pr4-l mb2 mb0-l">
                    <span htmlFor="cost" className="b pb2 db">Kopējā summa</span>
                    <input type="text" disabled name="cost" id="cost" min="1" className="pa2 w-100 trans" value={this.cost+" (€)"} />
                  </div>

                  <div className="w-100 w-50-m w-25-l">
                    <span htmlFor="cost2" className="b pb2 db">Kopējā summa ar PVN</span>
                    <input type="text" disabled name="cost2" id="cost_final" min="1" className="pa2 w-100" value={(this.cost+(this.cost*0.21))+" (€)"} />
                  </div>
                </div>
                <div className="mt4 <mt5-l></mt5-l>">
                <button onClick={() => { storeData.addToCart(this.prodId, this.addedProduct, this.paletes) }} className="w-100 w-auto-l pv3 ph4 br2 fr button--blue dib no-border cp t25">Pievienot grozam</button>
                </div>
              </div>
              
            )
          }
        </StoreContext.Consumer>

        <FloatingCart />
        </>
      )
    }
  }

}

export default Calculator