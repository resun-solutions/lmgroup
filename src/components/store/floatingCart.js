import React from "react"
import "./floatingCart.scss"
import { Link } from 'gatsby'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import StoreContext from "../../context/storeContext"

library.add(fas)

const FloatingCart = () => (
    <StoreContext.Consumer>
        {
            storeData => (
                <Link to="/veikals/grozs" className={storeData.cart.length===0?"floating-cart-hidden":"floating-cart"}>
                    <span className="cart-counter main-f b">{storeData.cart.length}</span>
                    <FontAwesomeIcon icon={['fas', 'shopping-cart']}  />
                </Link>
            )
        }
    </StoreContext.Consumer>
)


export default FloatingCart
