import React from "react"
import "./addToCart.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import StoreContext from "../../context/storeContext"

library.add(fas)

class AddToCart extends React.Component{
    
    constructor(props) {
        super(props);

        this.itemsToAdd = 1;
    }

    setItemsToAdd = (x) => {
        this.itemsToAdd += x;

        if(this.itemsToAdd == 0) {
            this.itemsToAdd = 1;
        }
        this.forceUpdate();
    }

    render() {
        return (
            <StoreContext.Consumer>
                {
                    storeData => (
                        <>
                        <div className="flex sec-font ">
                            <button onClick={()=>{this.setItemsToAdd(-1)}} className="cp w-third bg--lightgray f3 pa3 tc">-</button>
                            <div className="w-third pa3 tc f3 border-light">{this.itemsToAdd}</div>
                            <button onClick={()=>{this.setItemsToAdd(1)}} className="cp w-third pa3 f3 bg--lightgray tc">+</button>
                        </div>
                        <button onClick={()=>{ storeData.addToCart(this.props.id, this.props.cid, this.itemsToAdd) }} className="pa3 f4 tc font-sec bg--black w-100 no-border cp sec-font" style={{color:'white'}}>
                            <FontAwesomeIcon icon={['fas','cart-plus']}/> Pievienot grozam
                        </button>
                        </>
                    )
                }
            </StoreContext.Consumer>
        )
    }

} 



export default AddToCart
