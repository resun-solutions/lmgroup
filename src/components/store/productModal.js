import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Overlay from "../overlay/overlay"
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import StoreContext from "../../context/storeContext"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import AddToCart from "../../components/store/addToCart"

import "./style.scss"
library.add(fas)

class ProductModal extends React.Component {

constructor(props) {
  super(props);
}

render () {
  
    let item = this.props.products[''+this.props.id];
    console.log(item)
    if(item!=undefined) {
        return (
        <StoreContext.Consumer>
            {
            storeData => (
            <div className={storeData.viewItemId == -1 ? 'max-hide': ''}>
                <div className="fixed t0 l0 w-100 h-100 item-modal-wrapper flex flex-column items-center justify-start justify-center-l">

                    <button title="Aizvērt produktu" onClick={()=>{storeData.viewItem(-1)}} className="cp close-product bg-black white no-border f3 fixed t0 r0 w-100 w-auto-l w-auto-m"><FontAwesomeIcon icon={['fas','times']}/></button>

                    <div className="flex flex-column flex-column-reverse flex-row-l w-100 w-60-l pa4 pa0-l">
                        <div className="w-100 w-50-l tl pr4-l">
                            <h2 className="tc tl-l">AeroBlock D{item.density} {item.dx}x{item.dy}x{item.dz}</h2>
                            <p className="tj tl-l">
                            Sienas autoklāvētais gāzbetona bloks – mūsdienīgs, ekoloģiski tīrs celtniecības materiāls. Veselībai drošs, neietekmē eko-sistēmas līdzsvaru. Ērts lietošanā: materiālu viegli apstrādāt pat ar rokas instumentiem. Labi saglabā siltumu un rada komfortu telpās. Piemīt skaņas izolācijas spējas. Izmantojams gan privātmāju, gan komercbūvju būvniecībai. Labā bloku izmēru precizitāte garantē sienu geometrijas prezicitāti.
                            </p>
                            <p className="yellow b mb1">Cena par m<sup>3</sup></p>
                            <span className="b">{item.cost} EUR bez PVN</span>
                            <hr className="mv2"/>
                            <div className="flex flex-wrap sec-font mb4">
                                
                                <div className="w-50">Platums, mm</div>
                                <div className="w-50">{item.dx}</div>

                                <div className="w-50">Augstums, mm</div>
                                <div className="w-50">{item.dy}</div>

                                <div className="w-50">Garums, mm</div>
                                <div className="w-50">{item.dz}</div>

                                <div className="w-50">Svars, kg</div>
                                <div className="w-50">{item.weight}</div>

                                <div className="w-50">Bloka blīvums</div>
                                <div className="w-50">{item.density}</div>
                            </div>
                            <AddToCart id={this.props.id} cid={this.props.cid} />
                        </div>
                        <div className="w-100 w-50-l">
                        <img alt="Produkta attēls" className="border-light" src={require('../../images/store/'+this.props.id+".jpg")} />
                        <div className="random-square"></div>
                        </div>
                    </div>
                </div>
                </div>
            )
            }
            </StoreContext.Consumer>
        )
    }else {
        return '';
    }
}

}

export default ProductModal