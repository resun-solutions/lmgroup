import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import "./style.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import StoreContext from "../../context/storeContext"

library.add(fas)

class ProductItem extends React.Component {

constructor(props) {
  super(props);
}

render () {
  let d = this.props.data
  return (
    <StoreContext.Consumer>
      {
        storeData => (
          <div className="w-100 w-50-ns w-third-l flex flex-column pa0 pa2-ns pa4-l mb3 mb0-l product-item cp">
            <img alt="Produkta attēls" role="button" onClick={()=>{ storeData.viewItem(d.productId) }} src={require('../../images/store/'+d.productId+".jpg")} className="w-100 thumbnail--large" />
            <h3>
              AeroBlock D{d.density}, {d.dx}X{d.dy}X{d.dz}
            </h3>

            <div className="flex">
              <div className="w-50 sec-font tl">
                Bloka Izmēri, (P-A-G) mm
              </div>
              <div className="w-50 sec-font tr">
                Blīvums
              </div>
            </div>
            <hr className="mv1" />

            <div className="flex">
              <div className="w-50 sec-font tl">
                {d.dz+'–'+d.dy+'-'+d.dz}
              </div>
              <div className="w-50 sec-font tr">
                D{d.density}
              </div>
            </div>

            <div className="flex flex-column flex-row-l">
              <div className="w-100 w-50-l">
                <button onClick={()=>{ storeData.viewItem(d.productId) }} className="button pv3 ph4 mt3 button--gray w-100">Skatīt Produktu</button>
              </div>
              <div className="w-100 w-50-l">
              <button title="Pievienot grozam" onClick={()=>{ storeData.addToCart(d.productId, this.props.cid) }} className="button pv3 ph4 mt3 button--blue w-100">
                <FontAwesomeIcon icon={["fas","cart-plus"]}/>
              </button>
              </div>
            </div>
          </div>
        )
      }
      </StoreContext.Consumer>
  )
}

}

export default ProductItem