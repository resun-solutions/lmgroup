import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import ProductItem from "./productItem2"
import "./style.scss"
import StoreContext from "../../context/storeContext"
import FloatingCart from "./floatingCart"

import ProductModal from "../store/productModal"

class Store extends React.Component {

constructor(props) {
  super(props);
  this.productList = null
  this.cid = null
  
  this.props.data.edges.forEach((element, index) => {
      if(element.node.path == this.props.path) {
          this.productList = element.node.products
          this.cid = index
      }
  });
}


render () {
    return (
    <StoreContext.Consumer>
      {
        storeData => (
            <div id="store-wrapper" className="pa3 mt5 mt6-l tc">
            <h2>Izvēlies un pasūti jau tagad</h2>
            <p>Pilnām autokravām bezmaksas piegāde visā Latvija teritorijā (bez izkraušanas)</p>
            <div className="flex flex-wrap flex-column flex-row-ns">
              {
                  this.productList.map((edge) => (
                      <ProductItem cid={this.cid} data={edge} />
                  )
                  )
              }
            </div>
            <div className="item-modal">
              <ProductModal products={this.productList} cid={this.cid} id={storeData.viewItemId} />
            </div>
            <FloatingCart />
          </div>
        )
      }
    </StoreContext.Consumer>
  )
}

}

export default Store


