import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import PageHeader from "../components/navbar/header"
import ActionBanner from "../components/action-banner/action-banner"
import ServicesBlock from "../components/objects/services-block"

import { Parallax } from 'react-parallax';

import "./subpages.scss"

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allCostsJson {
        edges {
          node {
              category_title
              items {
                  service
                  cost
              }
          }
        }
      }
    }
  `);

    return (
    <Layout ru>
        <SEO 
        title="Plaša klāsta būvniecības pakalpojumi" 
        description="Sākot ar pamatu betonēšanu un beidzot ar teritorijas labiekārtošanu. Lm Group Buve piedāvā visdažādākos pakalpojumus."
        />
        
        <PageHeader heading="Услуги" url={"services-background.jpg"} />

        <div id="pakalpojumi" className="container mb0 mb5-m pt5 pt6-ns flex flex-wrap justify-around">

            <ServicesBlock  icon="betonesana.svg" heading="Фундаментные работы">
                Мы закладываем фундамент для любого частного дома, гаража, сада или дачи. Начиная с измерения фундамента и выкапывания ямы до изоляции, гидроизоляции.
            </ServicesBlock>

            <ServicesBlock  icon="muresana.svg" heading="Работы по кладке стен">
                Мы строим стены и перегородки из любого типа блоков. Размеры от 100мм до 500мм.
            </ServicesBlock>

            <ServicesBlock 
                icon="parsegumi.svg"
                heading="Установка междуэтажных перекрытий">
                Строим деревянные, железобетонные плиты или монолитные железобетонные плиты. Мы устанавливаем деревянные балки, готовые окна, а также мансардные окна VELUX и ROTO.
            </ServicesBlock>
            
            <ServicesBlock 
                icon="jumts.svg"
                heading="Сборка различных конструкций крыши">
                Монтаж стропил, мауэрлат, «кресла» крыши (укрепление стропил), установка мансардных окон, ветровых коробок, водосточных желобов. Возможность установки дополнительных элементов крыши – капельники, снегобарьеры, конёк крыши, ветровые кромки.
            </ServicesBlock>
            
            <ServicesBlock 
                icon="privatmaja.svg"
                heading="Строительство частных домов (A-Z)">
                Мы строим различные типы частных домов от A-Z. От основ до ключа. Подавайте заявку на проект!
            </ServicesBlock>
            <ServicesBlock 
                icon="ekovate.svg"
                heading="Монтаж эковаты и каменной ваты">
                Тёплый дом это основа для комфортной жизни, именно поэтому важно установить эковату или каменную вату.
            </ServicesBlock>
            <ServicesBlock 
                icon="brugesana.svg"
                heading="Укладка брусчатки и благоустройство территории">
                    Укладываем брусчатку для парковок к магазину или частному дому. Укладка тротуара, террасы, установка заборов.
            </ServicesBlock>
            <ServicesBlock 
                icon="apzalumosana.svg"
                heading="Озеленение территории">
                    Красивая, зелёная трава на всей территории!
            </ServicesBlock>

        </div>

        <ActionBanner url='request-background.jpg' title="Подать заявку на проект" subtitle="Консультируйтесь по любому вопросу" cta="Продолжить" href="email" />

        <div id="izcenojums" className="container mb0 mb6-ns">
            <Header color="yellow" showLine>Ценообразование услуг</Header>
            <p className="mb5-ns">
                Все цены указаны без НДС
                <span className="red-text">Цена рабочей силы не включает налоги, транспортные и машинные расходы!</span>
            </p>

            <div className="cost-table">

                <div className="header flex flex-wrap b ms-bold justify-start mb3">

                    {
                        data.allCostsJson.edges.map((edge, id) => (
                            <div className="w-100 w-50-ns flex flex-column mb5">
                                <h3 className={id%2==0?"ms-bold":"ml5-ns ms-bold"}>{edge.node.category_title}</h3>

                                <div className="flex mb2-ns">
                                    <div className={id%2==0?"w-50 mb2":"w-50 mb2 ml5-ns"}>Pakalpojums</div>
                                    <div className={id%2==0?"w-50 mb2 tr pr5-ns":"w-50 mb2 tr pr0-ns"}>Izmaksas</div>
                                </div>

                                <div className="flex flex-wrap justify-around">
                                    {
                                        edge.node.items.map((item, id2) => {
                                            return (
                                                <div className="w-100 grow main-font cost-table-row mb3 pen">
                                                    <div className={id%2==0?"row-border flex mr5-ns pb1-ns":"row-border flex ml5-ns pb1-ns"}>
                                                        <div className="w-50 sec-font">{item.service}</div>
                                                        <div className="normal w-50 tr b sec-font">{item.cost}</div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        ))
                    }

                </div>

                    {/* <div className="w-50 flex">
                        <div className="w-50">Pakalpojums</div>
                        <div className="w-50 tr pr5-ns">Izmaksas</div>
                    </div>

                    <div className="w-50 flex">
                        <div className="w-50 pl5-ns">Pakalpojums</div>
                        <div className="w-50 tr">Izmaksas</div>
                    </div> */}
                {/* <div className="flex flex-wrap justify-around">
                    {
                        data.allCostsJson.edges.map((edge, id) => {
                            return (
                                <div className="w-50 grow main-font cost-table-row mb2">
                                    <div className={id%2==0?"row-border flex mr5-ns":"row-border flex ml5-ns"}>
                                        <div className="w-50">{edge.node.service}</div>
                                        <div className="blue w-50 tr b">{edge.node.cost}</div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div> */}


            </div>
        </div>

    </Layout>
    )
}

export default IndexPage


