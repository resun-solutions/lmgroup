import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import Img from "gatsby-image"
import PageHeader from "../components/navbar/header"
import ActionBanner from "../components/action-banner/action-banner"

import "./subpages.scss"

class threeD extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {

    return (
    <Layout en>
        <SEO 
        title="Visualization of 3D ideas" 
        description="Do you have an idea you want to implement? We will help to improve it by offering you the most suitable solution."
        />
        
        <PageHeader heading="3D VISUALIZATION" url="3d-background.png" />

        <div id="about" className="container mb6-ns mt0 mt5-ns ">
            <div className="flex flex-column flex-row-l ">
                <div className="w-100 w-60-l ">
                    <Header color="yellow" showLine>3D VISUALIZATION OF YOUR IDEA</Header>
                    <p className="db">
                    If you have an idea, such as building a sauna, 3D visualization will help you understand what you really want and whether the way you have imagined in your mind corresponds to what it looks like in life.
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mt4 mb0-l ">
                    <img data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500" style={{maxWidth: '70%'}} src={require('../images/3d.jpg')} alt="LM GROUP BUVE logo" />
                </div>
            </div>
        </div>

        <div className="flex flex-wrap">
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/1.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/2.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/3.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/5.png')}/>
            </div>

            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/6.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/7.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/9.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/8.png')}/>
            </div>
        </div>

        <div className="flex flex-wrap container pb5 mb4-ns">
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                    <Header color="yellow" showLine>CREATION BY PLAN OR DRAWING</Header>
                    <p className="db">
                        We will create a model that meets your requirements and measurements. If you have a drawing or you have measured and created a drawing yourself, let's do a 3d visualization of this plan.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="150" data-sal-duration="500">
                    <Header color="yellow" showLine>REALISTIC LIGHTING</Header>
                    <p className="db">
                        3D visualization will help you understand what the room will look like in daylight, dawn, dusk or even at night.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="250" data-sal-duration="500">
                    <Header color="yellow" showLine>INTERIOR DESIGN</Header>
                    <p className="db">
                        With the help of 3D, it is possible to see what the room will look like in different shades and choose the most suitable color and material accordingly. This applies to walls, floors, as well as furniture and other elements.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center" data-sal="fade-in" data-sal-delay="350" data-sal-duration="500">
                <div className="wrapper w-70-ns">
                    <Header color="yellow" showLine>INDIVIDUAL SOLUTIONS</Header>
                    <p className="db">
                        We have faced a wide range of problems and answered a wide range of questions. We will help you realize your idea from start to finish.
                    </p>
                </div>
            </div>
        </div>

        <ActionBanner url='request-background.jpg' title="Apply for visualization" subtitle="We will help you find the most suitable solution" cta="Continue" href="email" />

        <div id="about" className="container mb5 mb6-ns flex items-center flex-column pt5 pt6-ns">
            <Header color="yellow" center>FURNITURE TO ORDER</Header>
            <div className="dib w-100 w-50-ns flex items-center flex-column">
                <p className="pb4 pb5-ns db mb0 tc">
                    In addition to visualizing 3D ideas, we also offer built-in and cabinet furniture according to individual needs.
                </p>
                <Link className="button w-100 w-auto-l tc button--blue cp br-10 pv3 ph4 relative dib mb0" to="/mebeles" data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500">Узнать больше</Link>
            </div>

        </div>

    </Layout>
    )
    }
}

export default threeD

export const query = graphql`
{
  allProjectsJson {
    edges {
      node {
        work
        title
        img {
          publicURL
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`