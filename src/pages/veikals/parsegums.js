import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../../components/layout"
import Header from "../../components/typography/header" // Title header
import SEO from "../../components/seo"
import PageHeader from "../../components/navbar/header";

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout>
        <SEO 
        title="Pārsegums"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        
        <PageHeader heading="Pārsegums" url="about-background.jpg" />

        <div id="about" className="container mb1 mb3-ns">
            <Header color="yellow" showLine>Pārsegums TERIVA</Header>
            <p className="pb2 pb0-ns db">
                TERIVA ir patentēta starpstāvu pārsegumu sistēma, kas atbilst Eiropas standartam LST EN 15037 un ir marķēta ar CE marķējumu. <br/><br/>
              TERIVA pārsegumu sistēmas sastāv no vieglām dzelzsbetona sijām, dobiem betona blokiem vai polistirola blokiem ( TERIVA LIGHT sistēmā ), kā arī monolīta betona pārklājuma, kas tiek izveidots jau pēc pārseguma sistēmas elementu uzstādīšanas. Šāda pārseguma konstrukcija ir viegla, izturīga un energofektīva, kas atbilst А, А+ un А++ energoefektivitātes klases prasībām.
            </p>
        </div>

        <div id="details" className="container">

          <Header color="yellow" showLine>TERIVA saliekamo pārseguma sistēmu priekšrocības</Header>

          <p className="pb4 pb5-ns db">
              <b>Salīdzinājumā ar pārseguma plātnēm</b> <br/><br/>
              1. TERIVA pārsegumu iespējams uzstādīt manuāli, neizmantojot pacelšanas krānus un citus mehānismus. Pārsegums tiek veidots no mazgabarīta elementiem. Viegli: bloks (atkarīgs no pārseguma tipa) sver 2; 14 vai 18 kg, bet sija – 12 kg.<br/>
              2. Transportēšana. Ar standarta izmēra kravas auto 13,6 m garumā, iespējams nogādāt uz objektu līdz 500 m2 pārseguma elementu. Pārseguma plātnes līdz 70 m2.<br/>
              3. Nogādājot objektā TERIVA pārseguma elementus, tos var izlādēt manuāli, pārseguma plātnes tikai ar krāna vai pacēlāja palīdzību.<br/>
              4. TERIVA pārsegums ir daudz vieglāks nekā pārseguma plātnes (aptuveni 100 -120 kg/m2) tādēļ mājas sienām var izmantot vieglākus konstrukciju un siltumizolācijas materiālus.<br/>
              5. Ēku rekonstrukcijās pārseguma elementus montāžas vietā var piegādāt pa durvīm vai logiem. Savukārt, pārseguma plātnes rekonstrukcijas gadījumos, kad jumta konstrukcija ir esoša, nebūs iespējams piegādāt šādā veidā.<br/>
              6. Pārseguma plātnes darbojas kā atsevišķi elementi, kas bieži ir iemesls plaisām starp plātnēm un sienām. TERIVA konstrukcija ir kā viengabala, monolīts elements.<br/>
              7. Ar pārseguma plātnēm, kā ar standartizētu izmēru izstrādājumiem, ir sarežģīti izveidot objekta pārsegumu bez ievērojamiem plātņu pārpalikumiem, kas ievērojami paaugstina kopējās izmaksas. Veidojot TERIVA pārsegumu, pārpalikumi veidojas nenozīmīgi.<br/><br/>


              <b>Salīdzinājumā ar monolīt-betona pārsegumiem</b> <br/><br/>
              1. TERIVA pārsegumam nav nepieciešams veidņu izgatavošana, armatūras siešana un statņu izveidošana.<br/>
              2. Betonēta pārseguma darba un transporta izmaksas ir ievērojami augstākas.<br/>
              3. TERIVA pārsegums ir vieglāks par 100 -120 kg/m2.<br/>
              4. TERIVA pārsegumā izmanto izturīgāku armatūru. Karkasu izgatavo no auksti velmēta, ribota un gluda tērauda, ar plastiskuma pakāpi - 500 MPa, kas atbilst Vācijas standartam DIN 488. Šis karkass ir par 23% izturīgāks nekā A-III klases armatūra.<br/>
              5. Laba siltumizolācija:<br/>

              TERIVA 4,0/1 - 0,37 m2 K/W<br/>
              TERIVA 4,0/3; 6.0; 8.0 - 0,39 m2 K/W<br/>
              TERIVA LIGHT 4,0/1 - 3,51 m2 K/W
          </p>

          <div className="flex flex-column-sm">
            <div className="w-100 w-50-l">
              <img className="w-100" src='/parsegums_1.jpg' />
            </div>
            <div className="w-100 w-50-l">
              <img className="w-100" src='/parsegums_2.jpg' />
            </div>
          </div>

          <Header color="yellow" showLine>Rekonstruējami objekti</Header>
          <p className="pb4 pb5-ns db mt4-l">
              TERIVA saliekamie pārsegumi ir viens no labākajiem risinājumiem ēku rekonstrukcijā.<br/><br/>

              Nostiprināt vecos, esošos pārsegumus vai mainīt tos uz jauniem - tas ir sarežģīts process, kas saistīts ar ievērojamām izmaksām. Pārseguma paneļus caur logu ailēm nav iespējams padot, tāpat dzelzsbetona monolīto pārsegumu ir ļoti sarežģīti izveidot, jo esošās konstrukcijas ir nepieciešams demontēt, lai izveidotu pārseguma balstus un esošās konstrukcijas var neizturēt lielu šķidrā betona daudzumu, tādēļ
              Izmantojot TERIVA saliekamo pārsegumu sistēmas, to var izdarīt daudz ērtāk un lētāk.<br/><br/>

              TERIVA pārseguma montāžas posmi:<br/><br/>

              <ol>
                <li>Sienās izveido atveres "ligzdas" pārseguma siju ievietošanai</li>
                <li>Pārseguma sijas montē ik pēc 60 cm vai 45 cm (atkarībā no pārseguma veida)</li>
                <li>Sijas balsta uz statnēm</li>
                <li>Izvieto pārseguma blokus</li>
                <li>Pārseguma konstrukciju pārklāj ar betonu (Marka C20/25)</li>
              </ol>
          </p>

          <div className="flex flex-column-sm">
            <div className="w-100 w-50-l">
              <img className="pr3-l w-100" src='/parsegums_3.jpg' />
            </div>
            <div className="w-100 w-50-l">
              <img className="w-100 pl3-l" src='/parsegums_4.jpg' />
            </div>
          </div>

          <Header color="yellow" showLine>Pasūtīšana</Header>
          <p>
            Lai sagatavotu izmaksu aprēķinu, mums ir nepieciešams objekta projekts, pdf vai dwg. Balstoties uz projektu, mūsu inženieri izveidos pārseguma plāna projektu un ieteiks optimālāko risinājumu. Rasējumu, kopā ar objekta adresi un kontaktinformāciju, lūgums sūtīt uz mūsu e-pastu - <a className="underline" href="mailto:info@lmgroup.lv" title="Nosūti ziņu">info@lmgroup.lv</a>
          </p>
          <Header color="yellow" showLine>Piegāde, norēķini un termiņi</Header>
          <p className="pb4 pb5-ns db mt4-l">
            Piegādes pakalpojumu nodrošinām visā Latvijas teritorijā, tieši no ražotnes Lietuvā. Piegāde ir maksas pakalpojums un tā nav iekļauta produktu cenā. Piegādes cena ir atkarīga no objekta atrašanās vietas. Izkraušana nav iekļauta piegādes pakalpojumā. Kā alternatīvu risinājumu, piedāvājam piegādi organizēt pašiem. Šajā gadījumā būs nepieciešama transportlīdzekļa un tā vadītāja identifikācijas informācija, lai sagatavotu CMR. <br/><br/>
            Norēķini - priekšapmaksa. Apmaksas veids - pārskaitījums. 
            Termiņi - vidēji 5 darba dienas, jo standarta izmēru pārseguma elementi vienmēr ir noliktavas atlikumā, un lielāko daļu laika aizņem projekta detaļu saskaņošana un piemērota transporta meklēšana.
          </p>


        </div>



    </Layout>
  )
}

export default IndexPage
