import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../../components/layout"
import Header from "../../components/typography/header" // Title header
import SEO from "../../components/seo"
import PageHeader from "../../components/navbar/header";

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout en>
        <SEO 
        title="Pārsegums"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        
        <PageHeader heading="COVER TERIVA" url="about-background.jpg" />

        <div id="about" className="container mb1 mb3-ns">
            <Header color="yellow" showLine>COVER TERIVA</Header>
            <p className="pb2 pb0-ns db">
                TERIVA is a patented mezzanine floor system that complies with the European standard LST EN 15037 and is CE marked. <br/><br/>
                TERIVA floor systems consist of lightweight reinforced concrete beams, hollow concrete blocks or polystyrene blocks (in the TERIVA LIGHT system), as well as a monolithic concrete coating, which is created after the installation of the floor system elements. Such a floor construction is light, durable and energy efficient, which meets the requirements of energy efficiency class А, А + and А ++.
            </p>
        </div>

        <div id="details" className="container">

          <Header color="yellow" showLine>ADVANTAGES OF TERIVA FOLDING FLOOR SYSTEMS</Header>

          <p className="pb4 pb5-ns db">
              <b>Compared to floor slabs</b> <br/><br/>
              1. The TERIVA cover can be installed manually without the use of lifting cranes and other mechanisms. The cover is made of small elements. Easy: the block (depending on the type of floor) weighs 2; 14 or 18 kg, but the beam - 12 kg.<br/>
              2. Transportation. With a standard-sized truck 13.6 m long, it is possible to deliver a floor element of up to 500 m2 to the object. Slabs up to 70 m2.<br/>
              3. When delivering the TERIVA floor elements to the site, they can be unloaded manually, the floor slabs only with the help of a crane or hoist.<br/>
              4. TERIVA flooring is much lighter than floor slabs (approximately 100 -120 kg / m2), therefore lighter construction and thermal insulation materials can be used for house walls.<br/>
              5. In the reconstruction of buildings, floor elements can be delivered through doors or windows at the installation site. On the other hand, in case of roof slab reconstruction, when the roof structure is existing, it will not be possible to deliver in this way.<br/>
              6. Slab contraction joints should intersect at the openings for columns and should intersect at the openings for columns. TERIVA construction is like a one-piece, monolithic element.<br/>
              7. Slab contraction joints should intersect at the openings for columns, and should intersect at the openings for columns. When forming the TERIVA cover, residues are formed insignificantly.<br/><br/>


              <b>Compared to monolithic concrete floors</b> <br/><br/>
              1. TERIVA flooring does not require the production of formwork, binding of reinforcement and creation of columns.<br/>
              2. The labor and transport costs of a concrete floor are significantly higher.<br/>
              3. TERIVA cover is lighter than 100 -120 kg / m2.<br/>
              4. TERIVA flooring uses more durable reinforcement. The frame is made of cold-rolled, ribbed and smooth steel, with a degree of plasticity of 500 MPa, which complies with the German standard DIN 488. This frame is 23% stronger than Class A-III reinforcement.<br/>
              5. Good thermal insulation:<br/>

              TERIVA 4,0/1 - 0,37 m2 K/W<br/>
              TERIVA 4,0/3; 6.0; 8.0 - 0,39 m2 K/W<br/>
              TERIVA LIGHT 4,0/1 - 3,51 m2 K/W
          </p>

          <div className="flex flex-column-sm">
            <div className="w-100 w-50-l">
              <img className="w-100" src='/parsegums_1.jpg' />
            </div>
            <div className="w-100 w-50-l">
              <img className="w-100" src='/parsegums_2.jpg' />
            </div>
          </div>

          <Header color="yellow" showLine>OBJECTS TO BE RECONSTRUCTED</Header>
          <p className="pb4 pb5-ns db mt4-l">
              TERIVA prefabricated floors are one of the best solutions for building reconstruction.<br/><br/>

              Strengthening old, existing coverings or replacing them with new ones is a complex process with significant costs. Slab contraction joints should not intersect at the openings for columns and should intersect at the openings for columns and should intersect at the openings for columns. more convenient and cheaper. <br/><br/>

              TERIVA floor assembly stages:<br/><br/>

              <ol>
                <li>Openings are made in the walls for the insertion of "socket" beams</li>
                <li>Slab contraction joints should intersect at the openings for columns and should intersect at the openings for columns.</li>
                <li>The beams are supported on columns</li>
                <li>Place the floor blocks</li>
                <li>Slab contraction joints should intersect at the openings for columns (Mark C20 / 25).</li>
              </ol>
          </p>

          <div className="flex flex-column-sm">
            <div className="w-100 w-50-l">
              <img className="pr3-l w-100" src='/parsegums_3.jpg' />
            </div>
            <div className="w-100 w-50-l">
              <img className="w-100 pl3-l" src='/parsegums_4.jpg' />
            </div>
          </div>

          <Header color="yellow" showLine>ORDERING</Header>
          <p>
            To prepare a cost estimate, we need a project of the object, pdf or dwg. Based on the project, our engineers will create a draft floor plan and recommend the optimal solution. Please send the drawing, together with the object address and contact information, to our e-mail - <a className="underline" href="mailto:info@lmgroup.lv" title="Nosūti ziņu">info@lmgroup.lv</a>
          </p>
          <Header color="yellow" showLine>DELIVERY, PAYMENT AND TERMS</Header>
          <p className="pb4 pb5-ns db mt4-l">
            We provide delivery services throughout the territory of Latvia, directly from the factory in Lithuania. Delivery is a paid service and is not included in the price of the products. The delivery price depends on the location of the object. Unloading is not included in the delivery service. As an alternative solution, we offer to arrange delivery ourselves. In this case, the vehicle and driver identification information will be required to prepare the CMR.
          </p>

          <p className="pb4 pb5-ns db mt4-l">
          Billing - prepay. Payment method - transfer. Deadlines - an average of 5 working days, because the standard size floor elements are always in stock, and most of the time is spent coordinating the project details and finding a suitable transport.
          </p>

        </div>



    </Layout>
  )
}

export default IndexPage
