import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../../components/layout"
import Header from "../../components/typography/header" // Title header
import SEO from "../../components/seo"
import PageHeader from "../../components/navbar/header";

import Parsedze from "../../images/store/1/parsedze.png"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout en>
        <SEO 
        title="Pārsedzes"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        
        <PageHeader heading="Overlays" url="about-background.jpg" />

        <div id="about" className="container mb5 mb5-l">
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-60-l">
            		<Header color="yellow" showLine>ABOUT OUR FINDINGS</Header>
                    <p className="pb4 pb5-ns db">
                    	We have been producing aerated concrete pavements in Latvia for 5 years, we have improved and optimized the production processes, obtaining much more durable and high-quality finished aerated concrete pavements. <br/> <br/>
                      Finished aerated concrete slabs greatly facilitate the construction process, thus reducing unnecessary labor costs and not wasting unnecessary time on construction by creating the slabs themselves from U Blocks or monolith. Finished aerated concrete coverings do not lag behind in terms of costs, if they are made on site, from U Blocks or formwork reinforced with concrete
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mb0-l mt6-l">
                    <img style={{maxWidth: '70%'}} src={Parsedze} alt="Attēls ar pārsedzi" />
                </div>
            </div>

            <a className ="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib" href="/faili/parsedzu_izcenojums.pdf" target="_blank" download title="Gāzbetona Bloku Izcenojums">
            <FontAwesomeIcon icon={['fas','download']} /> View prices</a>

            <a className ="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib ml3-l mt2 mt0-l" href="/faili/dekleracijas/parsedzu-dekleracija.pdf" target="_blank" download title="Pārsedžu Deklerācijas">
            <FontAwesomeIcon icon={['fas','download']} /> Download the declaration</a>

        </div>


{/* FEATURES SECTION */}
    <div className="mb0 mb5-m mb6-l">
      <Header color="yellow" showLine center>Advantages of ready-made lintels made of aerated concrete</Header>
     
      <div className="services-wrapper container cf flex flex-wrap justify-center justify-start-l mb5">
        
        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="50" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - 15 gadu pieredze" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/speed_yellow.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Fast and profitable</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="150" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Kvalitāte" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/safe.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Convenient and safe installation</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="300" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Ātra izpilde" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/quality_yellow.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Highest bearing capacity and deflection</h3>
            </div>
          </div>
        </div>


      </div>

    </div>
    {/* END FEATURES SECTION */}


    </Layout>
  )
}

export default IndexPage
