import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../../components/layout"
import Header from "../../components/typography/header" // Title header
import SEO from "../../components/seo"
import PageHeader from "../../components/navbar/header";

import Parsedze from "../../images/store/1/parsedze.png"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout ru>
        <SEO 
        title="Pārsedzes"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        
        <PageHeader heading="Перемычки" url="about-background.jpg" />

        <div id="about" className="container mb5 mb5-l">
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-60-l">
            		<Header color="yellow" showLine>О наших перемычках</Header>
                    <p className="pb4 pb5-ns db">
                    	Мы производим газобетонные перемычки в Латвии уже 5 лет. Мы улучшили и оптимизировали производственные процессы, получив гораздо более прочные и качественные готовые газобетонные перемычки.
Создавая самим перемычки из U блоков или монолита можно значительно облегчить и ускорить процесс строительства, тем самым сократить затраты на работу и не тратить лишнее время на строительство. Готовые газобетонные перемычки по цене так же выгодны, как и если создаются на месте на объекте из U блоков или из заармированой и залитой бетоном опалубки.
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mb0-l mt6-l">
                    <img style={{maxWidth: '70%'}} src={Parsedze} alt="Attēls ar pārsedzi" />
                </div>
            </div>

            <a className ="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib" href="/faili/parsedzu_izcenojums.pdf" target="_blank" download title="Gāzbetona Bloku Izcenojums">
            <FontAwesomeIcon icon={['fas','download']} /> Посмотреть цены</a>

            <a className ="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib ml3-l mt2 mt0-l" href="/faili/dekleracijas/parsedzu-dekleracija.pdf" target="_blank" download title="Pārsedžu Deklerācijas">
            <FontAwesomeIcon icon={['fas','download']} /> Скачать декларацию</a>

        </div>


{/* FEATURES SECTION */}
    <div className="mb0 mb5-m mb6-l">
      <Header color="yellow" showLine center>Преимущества готовых перемычек из газобетона</Header>
     
      <div className="services-wrapper container cf flex flex-wrap justify-center justify-start-l mb5">
        
        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="50" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - 15 gadu pieredze" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/speed_yellow.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Быстро и выгодно</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="150" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Kvalitāte" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/safe.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Удобная и безопасная установка</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="300" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Ātra izpilde" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/quality_yellow.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Наибольшая несущая способность и прогиб</h3>
            </div>
          </div>
        </div>


      </div>

    </div>
    {/* END FEATURES SECTION */}


    </Layout>
  )
}

export default IndexPage
