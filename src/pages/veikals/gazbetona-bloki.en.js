import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../../components/layout"
import Header from "../../components/typography/header" // Title header
import SEO from "../../components/seo"
import PageHeader from "../../components/navbar/header";

import Bloks from "../../images/store/block.jpg"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout en>
        <SEO 
        title="Blocks"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        
        <PageHeader heading="Blocks" url="about-background.jpg" />

        <div id="about" className="container mb5 mb5-l">
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-60-l">
            		<Header color="yellow" showLine>ABOUT OUR AERATED CONCRETE BLOCKS</Header>
                    <p className="pb4 pb5-ns db">
                    	Aeroblock autoclaved aerated concrete blocks are manufactured with German Wehrhahn production equipment. These equipment provide the highest quality aerated concrete block and production process control. The Aeroblock production process uses environmentally and health-friendly raw materials.
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mb0-l mt7-l">
                    <img style={{maxWidth: '100%'}} src={Bloks} alt="Gāzbetona bloks" />
                </div>
            </div>

			<a className ="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib" href="/faili/bloku_izcenojums.pdf" target="_blank" download title="Gāzbetona Bloku Izcenojums">
            <FontAwesomeIcon icon={['fas','download']} /> Check prices</a>

            <a className ="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib ml3-l mt2 mt0-l" href="/faili/dekleracijas/bloku-dekleracija.pdf" target="_blank" download title="Gāzbetona Bloku Deklerācijas">
            <FontAwesomeIcon icon={['fas','download']} /> Download declerations</a>

        </div>


{/* FEATURES SECTION */}
    <div className="mb0 mb5-m mb6-l">
      <Header color="yellow" showLine center>Why choose our blocks</Header>
     
      <div className="services-wrapper container cf flex flex-wrap justify-center justify-start-l mb5">
        
        <div className="w-100-m w-third-ns mb5 mb0-l">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="50" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - 15 gadu pieredze" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/eko.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">EKO friendly</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns mb5 mb0-l">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="150" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Kvalitāte" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/sound.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Soundproofing</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns mb5 mb0-l">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="300" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Ātra izpilde" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/usage.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Easy to work with</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns mt6-l mb5 mb0-l">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="150" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Kvalitāte" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/cold.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Frost resistance</h3>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns mt6-l">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="300" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Ātra izpilde" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/warm.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Energy efficiency</h3>
            </div>
          </div>
        </div>


      </div>

    </div>
    {/* END FEATURES SECTION */}


    </Layout>
  )
}

export default IndexPage
