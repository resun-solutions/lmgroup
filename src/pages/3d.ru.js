import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import Img from "gatsby-image"
import PageHeader from "../components/navbar/header"
import ActionBanner from "../components/action-banner/action-banner"

import "./subpages.scss"

class threeD extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {

    return (
    <Layout ru>
        <SEO 
        title="3D ideju vizualizācija" 
        description="Vai tev ir kāda ideja, kuru vēlies īstenot? Mēs to palīdzēsim pilnveidot, piedāvājot Tev vispiemērotāko risinājumu."
        />
        
        <PageHeader heading="3D визуализация" url="3d-background.png" />

        <div id="about" className="container mb6-ns mt0 mt5-ns ">
            <div className="flex flex-column flex-row-l ">
                <div className="w-100 w-60-l ">
                    <Header color="yellow" showLine>3D визуализация вашей идеи</Header>
                    <p className="db">
                    Если у вас есть идея, например строительство сауны, 3D-визуализация поможет вам понять, чего вы действительно хотите, и соответствуют ли ваши ожидания реальности.
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mt4 mb0-l ">
                    <img data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500" style={{maxWidth: '70%'}} src={require('../images/3d.jpg')} alt="LM GROUP BUVE logo" />
                </div>
            </div>
        </div>

        <div className="flex flex-wrap">
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/1.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/2.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/3.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/5.png')}/>
            </div>

            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/6.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/7.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/9.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/8.png')}/>
            </div>
        </div>

        <div className="flex flex-wrap container pb5 mb4-ns">
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                    <Header color="yellow" showLine>Создание по плану или чертежу</Header>
                    <p className="db">
                        Мы создадим модель, которая соответствует вашим требованиям и измерениям. Если у вас есть чертеж или вы измерили и создали чертеж самостоятельно, давайте сделаем трехмерную визуализацию этого плана.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="150" data-sal-duration="500">
                    <Header color="yellow" showLine>Реалистичное освещение</Header>
                    <p className="db">
                        3D-визуализация поможет вам понять, как будет выглядеть комната при дневном свете, на рассвете, в сумерках или даже ночью.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="250" data-sal-duration="500">
                    <Header color="yellow" showLine>дизайн интерьера</Header>
                    <p className="db">
                        С помощью 3D можно увидеть, как комната будет выглядеть в разных оттенках, и соответственно выбрать наиболее подходящий цвет и материал. Это касается стен, полов, а также мебели и других элементов.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center" data-sal="fade-in" data-sal-delay="350" data-sal-duration="500">
                <div className="wrapper w-70-ns">
                    <Header color="yellow" showLine>Индивидуальные решения</Header>
                    <p className="db">
                        Мы столкнулись с широким кругом проблем и ответили на широкий круг вопросов. Мы поможем вам реализовать вашу идею от начала до конца.
                    </p>
                </div>
            </div>
        </div>

        <ActionBanner url='request-background.jpg' title="Подать заявку на визуализацию" subtitle="Мы поможем вам найти наиболее подходящее решение" cta="Продолжать" href="email" />

        <div id="about" className="container mb5 mb6-ns flex items-center flex-column pt5 pt6-ns">
            <Header color="yellow" center>Мебель на заказ</Header>
            <div className="dib w-100 w-50-ns flex items-center flex-column">
                <p className="pb4 pb5-ns db mb0 tc">
                    Для дополнительной визуализации 3D-идей мы также предлагаем встроенную и корпусную мебель в соответствии с индивидуальными потребностями.
                </p>
                <Link className="button w-100 w-auto-l tc button--blue cp br-10 pv3 ph4 relative dib mb0" to="/mebeles" data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500">Узнать больше</Link>
            </div>

        </div>

    </Layout>
    )
    }
}

export default threeD

export const query = graphql`
{
  allProjectsJson {
    edges {
      node {
        work
        title
        img {
          publicURL
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`