import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import PageHeader from "../components/navbar/header";

const NotFoundPage = () => {

  return (
    <Layout>
        <SEO 
        title="Par uzņēmumu un projektu gaitu"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        <PageHeader heading="404" url="landing-background.jpg" />

        <div id="shop" className="container mb5 mb6-l">
            <Header color="yellow" showLine>Meklētā lapa neeksistē</Header>
            <Link className="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib" to="">Uz sākumu</Link>
        </div>


    </Layout>
  )
}

export default NotFoundPage
