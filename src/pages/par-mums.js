import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../components/layout"
import Img from "gatsby-image"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import RigaImage from "../images/riga.png"
import "./subpages.scss"
import ActionBanner from "../components/action-banner/action-banner";
import PageHeader from "../components/navbar/header";
import StepNumber from "../components/objects/step-number"
import Logo from "../images/gatsby-icon_cropped.png"
import { Parallax } from 'react-parallax';

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout>
        <SEO 
        title="Par uzņēmumu un projektu gaitu"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        
        <PageHeader heading="Par Mums" url="about-background.jpg" />

        <div id="about" className="container mb5 mb6-l">
            <Header color="yellow" showLine>Par LM GROUP BUVE</Header>
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-60-l">
                    <p className="pb4 pb5-ns db">
                    Uzņēmums LM Group Buve tika dibināts 2005. gada 10. janvārī un šobrīd jau 15. gadu veic gan lielus gan mazus būvprojektus Rīgā un Rīgas Rajonā. Mēs piedāvājam visus ar remontdarbiem un būvniecību saistītos risinājumus, galvenokārt privātmāju būvniecību. Mūsu profesionālā meistaru komanda parūpēsies, lai dzīvokļa vai privātmājas remonts vai būvniecība neprasītu no Tevis laiku, galvassāpes un liekus līdzekļus.<br /><br />
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mb0-l">
                    <img style={{maxWidth: '70%'}} src={Logo} alt="LM GROUP BUVE logo" />
                </div>
            </div>

            <Link className="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib" paintDrip hex={transitionHex} to="/pakalpojumi">Pakalpojumi</Link>

        </div>

        <Parallax bgImage={RigaImage} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={0}>
            <div className="map relative pv5 pv4-ns pv6-l flex flex-column content-center w-100 container overflow-hidden">
            <div className="pv4">
                <h2 className="white normal ms-light mb0 pb0">Strādājam Rīgā, <br/> <span className="yellow">15km</span> - <span className="yellow">45km</span> ap Rīgu.</h2><br/>
                <p className="white ms-light mt0 mb0">Vajadzības gadījumā izbraucam pa visu Latviju</p>
            </div>
            </div>
        </Parallax>


        <div id="project-steps" className="container mb3">
            <Header color="yellow" showLine center>Projekta Gaita</Header>
            <div className="flex justify-center">
                <ol className="ml0 ph0 ph4-ns flex flex-column list db w-90 w-70-l">

                <StepNumber heading="Būvprojekta Izskatīšana" number="1">
                    Kad esi ar mums sazinājies, izskatām būvprojektu, piedāvajot cenas, kuras ir norādītas mājaslapā (atkarīgs no pakalpojuma). Ja Tevi viss apmierina, izveidojam detalizētu tāmi tieši Tev. Tāme ir ļoti precīza un darba gaitā nemainās.
                </StepNumber>

                <StepNumber heading="Vienošanās par būvizmaksām" number="2">
                    Ja būvizmaksas tiek apstiprinātas un tāme izveidota, izrunājam apmaksas kārtību. Dažādos projektos un dažādās situācijās tā var būt atšķirīga, tāpēc mēs ar Tevi par to vienosimies.
                </StepNumber>

                <StepNumber heading="Būvlaukuma sagatavošana" number="3">
                    Pirms projekta uzsākšanas ir nepieciešami sagatavošanās darbi. Mēs sagādājam būvgružu konteineru, celtnieku vagoniņu un citas sadzīves un būvniecības nepieciešamības. 
                </StepNumber>

                <StepNumber heading="Būvniecības proces" number="4">
                    Sākam darbu! Katru nedēļu sūtām bildes no objekta, izrunājam vispārīgas lietas par projekta gaitu un problēmām, ja tādas ir radušās. Īsumā - Tu zināsi, kas notiek un cik tālu ir tavs projekts!
                </StepNumber>

                <StepNumber heading="Objekta sakopšana" number="5">
                    Objekts ir pabeigts, visi būvgruži savākti, objekts sakopts un tīrs. Mēs aizvācam konteineru, vagoniņu un visu pārējo.
                </StepNumber>

            </ol>
            </div>
        </div>

        <ActionBanner url='request-background.jpg' title="Piesaki projektu" subtitle="Konsultējies jebkurā jautājumā" cta="Pieteikties" href="email" />

        

        <div id="partners" className="container mb3 mb6-ns">
            <Header color="yellow" showLine>Sadarbības Partneri</Header>
            <div className="flex flex-wrap">
                {
                    data.allPartnersJson.edges.map((edge, id) => {
                        return (
                            <div className="w-50 w-20-ns ph3" data-sal="fade" data-sal-delay={50*(id-1)} data-sal-duration="1000">
                                <Img fluid={edge.node.img.childImageSharp.fluid} alt={edge.node.name+" - logo"} />
                            </div>
                        )
                    })
                }
            </div>
        </div>

    </Layout>
  )
}

export default IndexPage
