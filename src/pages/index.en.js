import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import Img from "gatsby-image"
import SubmitTestimonial from "../components/testimonials/submitTestimonial"
import ActionBanner from "../components/action-banner/action-banner"

import Thumbnail from "../images/more-thumbnail.jpeg"

import SocialBanner from "../images/social_banner.png"


import { Parallax } from 'react-parallax';
import "./index.scss"

const IndexPage = ({testM}) => {

  const data = useStaticQuery(graphql`
  {
    allProjectsJson {
      edges {
        node {
          work
          title
          img {
            publicURL
            childImageSharp {
              fluid(maxWidth: 800, maxHeight: 800) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  }
`);

  const transitionHex = "#0099ff";

return (
  <Layout en>
    <SEO 
    title="Private house construction, Apartment repair, Insulation" 
    description="LM GROUP BUVE is a construction company competing in Latvia, which has been implementing both large and small projects in various construction spheres for more than 15 years."
    />    

    {/* LANDING SECTION */}
    <Parallax blur={0} bgImage={require('../images/landing-background.jpg')} bgImageAlt="Attēls ar modernām ēkām" strength={600}>
      <div className="full-height-container landing-tint"></div>
      <div id="landing-header" className="full-height-container flex flex-column items-center justify-center absolute">
        <h1 className="ttu ms-black white">Construction of private houses <span className="yellow ms-black">A-Z</span></h1>
        <p className="white f4">Private house construction, Ecowool incorporation. Consultations. Procurement of materials.</p>

        <Link to="#services" className="button button--yellow cp black br-pill pv3 ph4 b relative" data-sal="zoom-in" data-sal-delay="50" data-sal-duration="300">Our services</Link>

      </div>
    </Parallax>

    {/* SERVICES SECTION */}
    <div id="services" className="pb4 pb6-ns">
      <Header color="yellow" showLine center>Our services</Header>
     
      <div className="services-wrapper container cf flex flex-wrap justify-between mb0 mb5-l">
        
        <div className="w-100 w-40-l pl2">
          <div className="services-item flex" data-sal="fade" data-sal-delay="50" data-sal-duration="1000">
            <img alt="Pakalpojuma Ilustrācija - Betonēšana" style={{
              width: '100%',
              height: '100%'
            }} src='/icons/betonesana.svg' />
            <div className="ml4 flex block-l flex-column justify-center">
              <h3 className="f4 f3-m ms-bold mb2">Foundation Concreting</h3>
              <p className="f6 f5-m">We lay the foundations for any private house, garage, shop or summer house</p>
            </div>
          </div>
        </div>

        <div className="w-100 w-40-l pl2">
          <div className="services-item flex" data-sal="fade" data-sal-delay="100" data-sal-duration="1000">
            <img alt="Pakalpojuma Ilustrācija - Mūrēšana" style={{
              width: '100%',
              height: '100%'
            }} src='/icons/muresana.svg' />
            <div className="ml4 flex block-l flex-column justify-center">
              <h3 className="f4 f3-m ms-bold mb2">Masonry work</h3>
              <p className="f6 f5-m">We build walls and partitions with any type of blocks</p>
            </div>
          </div>
        </div>

      </div>

      <div className="services-wrapper container cf flex flex-wrap justify-between">
        
        <div className="w-100 w-40-l pl2" data-sal="fade" data-sal-delay="150" data-sal-duration="1000">
          <div className="services-item flex">
            <img alt="Pakalpojuma Ilustrācija - Pārsegumi" style={{
              width: '100%',
              height: '100%'
            }} src='/icons/parsegumi.svg' />
            <div className="ml4 flex block-l flex-column justify-center">
              <h3 className="f4 f3-m ms-bold mb2">Construction of mezzanine floors</h3>
              <p className="f6 f5-m">We build wooden, reinforced concrete slabs or monolithic reinforced concrete coverings. We install wooden beams, finished windows, as well as VELUX and ROTO roof windows.</p>
            </div>
          </div>
        </div>

        <div className="w-100 w-40-l pl2">
          <div className="services-item flex" data-sal="fade" data-sal-delay="200" data-sal-duration="1000">
            <img alt="Pakalpojuma Ilustrācija - Jumtu montāža" style={{
              width: '100%',
              height: '100%'
            }} src='/icons/jumts.svg' />
            <div className="ml4 flex block-l flex-column justify-center">
              <h3 className="f4 f3-m ms-bold mb2">Installation of roof structures</h3>
              <p className="f6 f5-m">We build a roof from 0, as well as clean old roofs of moss</p>
            </div>
          </div>
        </div>

      </div>

    </div>
    {/* END SERVICES SECTION */}
    
    {/* COST SECTION */}
    <ActionBanner url='cost-background.jpg' title="Friendly costs" subtitle="We will agree on the project costs together" cta="View the cost table" href="pakalpojumi/#izcenojums" />

    {/* FEATURES SECTION */}
    <div className="mb0 mb5-m mb6-l">
      <Header color="yellow" showLine center>Why choose us</Header>
     
      <div className="services-wrapper container cf flex flex-wrap justify-between mb5">
        
        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="50" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - 15 gadu pieredze" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/15_years_yellow.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">More than 15 years experience</h3>
              <p>Our expert team is ready for all challenges</p>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="150" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Kvalitāte" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/quality_yellow.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Quality guaranteed</h3>
              <p>Starting with the selection of suitable materials and ending with a tidy object</p>
            </div>
          </div>
        </div>

        <div className="w-100-m w-third-ns">
          <div className="services-item flex flex-column items-center" data-sal="fade" data-sal-delay="300" data-sal-duration="1000">
            <img alt="Priekšrocību Ilustrācija - Ātra izpilde" style={{
              width: '100%',
              maxWidth: '100px',
              height: '100%'
            }} src="/icons/speed_yellow.svg" />
            <div className="ph4 tc">
              <h3 className="ms-bold mb2">Fast execution</h3>
              <p>A full-cycle private house project (foundations / wall / floor / roof) lasts on average up to 3 months</p>
            </div>
          </div>
        </div>


      </div>

    </div>
    {/* END FEATURES SECTION */}

    {/* Showcase */}
    <div className="showcase flex flex-wrap lh0 pb0 mb0 pt6-l">

      {
        data.allProjectsJson.edges.map((edge, id) => {
          if(id<4){
            return (
              <Link className="w-100 w-50-m w-20-l relative" paintDrip hex={transitionHex} to="/projekti">
                <Img alt="Bilde no objekta" fluid={edge.node.img.childImageSharp.fluid} />
                {/* <img className="ma0" src={Thumbnail} /> */}
                <div className="project-gradient flex flex-column justify-end pa3 white cp">
                    <span className="mh0 mb3 f6 ms-light o-70">
                      {
                        edge.node.work.map((item, id) => {
                          if(id < edge.node.work.length-1) {
                            return ""+item+" / "
                          }else {
                            return ""+item
                          }
                        })
                      }
                    </span>
                    <h3 className="ma0 f5 normal ttu" data-sal="slide-right" data-sal-delay={150*(id-1)} data-sal-duration="500">{edge.node.title}</h3>
                </div>
              </Link>
            )
          }
        })
      }

    
      <Link className="w-100 w-20-l h5 h-inherit-l relative overflow-hidden" paintDrip hex={transitionHex} to="/projekti">
        <div className="overflow-hidden blur-wrapper">
          <img alt="Izpludināts attēls bez nozīmes" className="ma0 blur-15" src={Thumbnail} />
        </div>
        <div className="project-gradient flex flex-column justify-center items-center pa3 white cp">
            <h3 className="ma0 f5 normal ttu">
              <span className="flex justify-center f3 mb2">
                <img alt="Bultiņa pa labi" src="/icons/chevron-right.svg" style={{ maxWidth: '50px' }} />
              </span>
              See all</h3>
        </div>
        </Link>

    </div>
    {/* End Showcase */}

    <div id="testimonials" className="container mb6">
        <SubmitTestimonial en />
    </div>

  </Layout>
  )
}

export default IndexPage
