import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import Img from "gatsby-image"
import PageHeader from "../components/navbar/header"
import ActionBanner from "../components/action-banner/action-banner"
import StoreContext from "../context/storeContext"
import "./subpages.scss"

class mebeles extends React.Component {

    render () {

    return (
        <StoreContext.Consumer>
            {
                storeData => (
                    <Layout en>
                        <SEO 
                        title="Visualization of 3D ideas" 
                        description="Do you have an idea you want to implement? We will help to improve it by offering you the most suitable solution."
                        />
                        
                        <PageHeader heading="Custom-made furniture" url="mebeles-background.png" />

                        <div id="about" className="container mb6-ns mt0 mt5-ns ">
                            <div className="flex flex-column flex-row-l ">
                                <div className="w-100 w-60-l ">
                                    <Header color="yellow" showLine>Built-in and cabinet furniture</Header>
                                    <p className="db">
                                        If you have an idea, such as building a sauna, 3D visualization will help you understand what you really want and if your expectations are in line with reality.
                                    </p>
                                </div>
                                <div className="w-100 w-40-l flex items-center justify-center mb3 mb5-l mt4 mb0-l ">
                                <img data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500"  className=" w-50" src='/icons/mebeles.svg' alt="LM GROUP BUVE logo" />
                                </div>
                            </div>
                        </div>

                        <div className="flex flex-wrap">

                            <div className="w-100 w-50-l lh0">
                                <img alt="Virtuve" className="ma0 pa0 lh0 w-100" src={require('../images/mebeles/1.jpg')}/>
                            </div>
                            <div className="w-100 w-50-l lh0 dn flex-l items-center justify-center" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                                <Header color="yellow" center>BUILT-IN CABINETS</Header>
                            </div>

                            <div className="w-100 w-50-l lh0 dn flex-l items-center justify-center" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                                <Header color="yellow" center>Kitchen furniture</Header>
                            </div>
                            <div className="w-100 w-50-l lh0">
                                <img alt="Virtuve" className="ma0 pa0 lh0 w-100" src={require('../images/mebeles/2.jpg')}/>
                            </div>

                            <div className="w-100 w-50-l lh0">
                                <img alt="Virtuve" className="ma0 pa0 lh0 w-100" src={require('../images/mebeles/3.jpg')}/>
                            </div>
                            <div className="w-100 w-50-l lh0 dn flex-l items-center justify-center" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                                <Header color ="yellow" center>Sliding doors</Header>
                            </div>

                            <div className="w-100 w-50-l lh0 dn flex-l items-center justify-center" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                                <Header color="yellow" center>Wardrobe</Header>
                            </div>
                            <div className="w-100 w-50-l lh0">
                                <img alt="Virtuve" className="ma0 pa0 lh0 w-100" src={require('../images/mebeles/4.jpg')}/>
                            </div>

                            <div className="w-100 w-50-l lh0">
                                <img alt="Virtuve" className="ma0 pa0 lh0 w-100" src={require('../images/mebeles/6.jpg')}/>
                            </div>
                            <div className="w-100 w-50-l lh0 dn flex-l items-center justify-center" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                                <Header color ="yellow" center>Shelving system (shelves)</Header>
                            </div>

                            <div className="w-100 w-50-l lh0 dn flex-l items-center justify-center" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                                <Header color="yellow" center>Partitions</Header>
                            </div>
                            <div className="w-100 w-50-l lh0">
                                <img alt="Virtuve" className="ma0 pa0 lh0 w-100" src={require('../images/mebeles/10.jpg')}/>
                            </div>

                            <div className="w-100 w-50-l lh0">
                                <img alt="Virtuve" className="ma0 pa0 lh0 w-100" src={require('../images/mebeles/9.jpg')}/>
                            </div>
                            <div className="w-100 w-50-l lh0 dn flex-l items-center justify-center" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                                <Header color ="yellow" center>Sliding doors</Header>
                            </div>


                        </div>

                        <div id="about" className="container mb5 mb6-ns flex items-center flex-column pt5 pt6-ns">
                            <Header color="yellow" center>Contact us and get an individual offer</Header>
                            <div className="dib w-100 w-50-ns flex items-center flex-column">
                                <p className="pb4 pb5-ns db mb0 tc">
                                    Everyone has their own conditions and the solution is tailored to individual needs. We will help you choose the best option.
                                </p>
                                <button title="Связаться с нами" onClick={storeData.toggleEmailPopup} data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500" className="button w-100 w-auto-l tc button--blue cp br-10 pv3 ph4 relative dib mb0" to="/mebeles">Contact us</button>
                            </div>

                        </div>

                    </Layout>
                )
            }
        </StoreContext.Consumer>
    )
    }
}

export default mebeles

export const query = graphql`
{
  allProjectsJson {
    edges {
      node {
        work
        title
        img {
          publicURL
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`