import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import PageHeader from "../components/navbar/header"
import ActionBanner from "../components/action-banner/action-banner"
import ServicesBlock from "../components/objects/services-block"

import { Parallax } from 'react-parallax';

import "./subpages.scss"

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allCostsJson {
        edges {
          node {
              category_title
              items {
                  service
                  cost
              }
          }
        }
      }
    }
  `);

    return (
    <Layout en>
        <SEO 
        title="Wide range of construction services" 
        description="Starting with concreting the foundations and ending with landscaping. Lm Group Buve offers a wide range of services."
        />
        
        <PageHeader heading="Services" url={"services-background.jpg"} />

        <div id="pakalpojumi" className="container mb0 mb5-m pt5 pt6-ns flex flex-wrap justify-around">

            <ServicesBlock  icon="betonesana.svg" heading="Concreting of foundations">
                We lay the foundations for any private house, garage, or cottage or summer house. Starting with measuring the foundation and digging a construction pit to insulation, waterproofing.
            </ServicesBlock>

            <ServicesBlock  icon="muresana.svg" heading="Masonry work">
                We build walls and partitions with any type of blocks. Sizes from 100mm to 500mm.
            </ServicesBlock>

            <ServicesBlock 
                icon="parsegumi.svg"
                heading="Construction of mezzanine floors">
                We build wooden, reinforced concrete slabs or monolithic reinforced concrete coverings. We install wooden beams, finished windows, as well as VELUX and ROTO roof windows.
            </ServicesBlock>
            
            <ServicesBlock 
                icon="jumts.svg"
                heading="Installation of roof structures">
                Installation of rafters, masonry and chairs, installation of roof windows, wind boxes, gutters. It is possible to install additional roof elements - lasenai, snow barriers, ridges and lawns.
            </ServicesBlock>
            
            <ServicesBlock 
                icon="privatmaja.svg"
                heading="Construction of private houses (A-Z)">
                We build various types of private houses from A-Z. From the basics to the key. Apply for a project!
            </ServicesBlock>
            <ServicesBlock 
                icon="ekovate.svg"
                heading="Ecowool and stone wool incorporation">
                Warm homes are the basis for a comfortable life and an important precondition for halting global warming. Let's work with eco-wool or rock wool, using the device intended for it.
            </ServicesBlock>
            <ServicesBlock 
                icon="brugesana.svg"
                heading="Paving and landscaping">
                    We will pave the parking lot for a shop or a private house. Paving of paths, construction of terraces, installation of fences.
            </ServicesBlock>
            <ServicesBlock 
                icon="apzalumosana.svg"
                heading="Greening">
                    Green, beautiful grass in the whole area.
            </ServicesBlock>

        </div>

        <ActionBanner url='request-background.jpg' title="Submit your project" subtitle="Consult about anything" cta="Submit" href="email" />

        <div id="izcenojums" className="container mb0 mb6-ns">
            <Header color="yellow" showLine>Pricing</Header>
            <p className="mb5-ns">
                All prices shown without VAT
                <span className="red-text">The cost does not cover taxes and the costs of mechanics!</span>
            </p>

            <div className="cost-table">

                <div className="header flex flex-wrap b ms-bold justify-start mb3">

                    {
                        data.allCostsJson.edges.map((edge, id) => (
                            <div className="w-100 w-50-ns flex flex-column mb5">
                                <h3 className={id%2==0?"ms-bold":"ml5-ns ms-bold"}>{edge.node.category_title}</h3>

                                <div className="flex mb2-ns">
                                    <div className={id%2==0?"w-50 mb2":"w-50 mb2 ml5-ns"}>Pakalpojums</div>
                                    <div className={id%2==0?"w-50 mb2 tr pr5-ns":"w-50 mb2 tr pr0-ns"}>Izmaksas</div>
                                </div>

                                <div className="flex flex-wrap justify-around">
                                    {
                                        edge.node.items.map((item, id2) => {
                                            return (
                                                <div className="w-100 grow main-font cost-table-row mb3 pen">
                                                    <div className={id%2==0?"row-border flex mr5-ns pb1-ns":"row-border flex ml5-ns pb1-ns"}>
                                                        <div className="w-50 sec-font">{item.service}</div>
                                                        <div className="normal w-50 tr b sec-font">{item.cost}</div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        ))
                    }

                </div>

                    {/* <div className="w-50 flex">
                        <div className="w-50">Pakalpojums</div>
                        <div className="w-50 tr pr5-ns">Izmaksas</div>
                    </div>

                    <div className="w-50 flex">
                        <div className="w-50 pl5-ns">Pakalpojums</div>
                        <div className="w-50 tr">Izmaksas</div>
                    </div> */}
                {/* <div className="flex flex-wrap justify-around">
                    {
                        data.allCostsJson.edges.map((edge, id) => {
                            return (
                                <div className="w-50 grow main-font cost-table-row mb2">
                                    <div className={id%2==0?"row-border flex mr5-ns":"row-border flex ml5-ns"}>
                                        <div className="w-50">{edge.node.service}</div>
                                        <div className="blue w-50 tr b">{edge.node.cost}</div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div> */}


            </div>
        </div>

    </Layout>
    )
}

export default IndexPage


