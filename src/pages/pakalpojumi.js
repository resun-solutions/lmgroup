import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import PageHeader from "../components/navbar/header"
import ActionBanner from "../components/action-banner/action-banner"
import ServicesBlock from "../components/objects/services-block"

import { Parallax } from 'react-parallax';

import "./subpages.scss"

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allCostsJson {
        edges {
          node {
              category_title
              items {
                  service
                  cost
              }
          }
        }
      }
    }
  `);

    return (
    <Layout>
        <SEO 
        title="Plaša klāsta būvniecības pakalpojumi" 
        description="Sākot ar pamatu betonēšanu un beidzot ar teritorijas labiekārtošanu. Lm Group Buve piedāvā visdažādākos pakalpojumus."
        />
        
        <PageHeader heading="Pakalpojumi" url={"services-background.jpg"} />

        <div id="pakalpojumi" className="container mb0 mb5-m pt5 pt6-ns flex flex-wrap justify-around">

            <ServicesBlock  icon="betonesana.svg" heading="Pamatu betonēšana">
                Lejam pamatus jebkurai privātmājai, garāžai, vaikalam vai vasarnīcai. Sākot ar pamatu iemērīšanu un būvbedres izrakšanu līdz siltināšanai, hidroizolācijai.
            </ServicesBlock>

            <ServicesBlock  icon="muresana.svg" heading="Mūrēšanas darbi">
                Mūrējam sienas un starpsienas ar jebkāda veida blokiem. Izmēri sākot ar 100mm līdz 500mm.
            </ServicesBlock>

            <ServicesBlock 
                icon="parsegumi.svg"
                heading="Starpstāvu pārsegumu izbūve">
                Izbūvējam koka, dzelsbetona plākšņu vai monolīta dzelsbetona pārsegumus. Montējam koka sijas, gatavos logus, kā arī VELUX un ROTO jumta logus.
            </ServicesBlock>
            
            <ServicesBlock 
                icon="jumts.svg"
                heading="Jumtu konstrukciju montāža">
                Spāru, mūrlatu un krēslu montāža, jumta logu, vēja kastu, notekreņu montāža. Iespējams uzstādīt jumta papildelementus - laseni, sniega barjeras, kores un vejmalas.
            </ServicesBlock>
            
            <ServicesBlock 
                icon="privatmaja.svg"
                heading="Privātmāju būvniecība (A-Z)">
                Būvējam dažāda veida privātmājas no A-Z. No pamatiem līdz atslēgai. Piesaki projektu!
            </ServicesBlock>
            <ServicesBlock 
                icon="ekovate.svg"
                heading="Ekovates un akmensvates iestrāde">
                Siltas mājas ir pamats komfortablai dzīvei un nozīmīgs priekšnosacījums globālās sasilšanas apturēšanai. Iestrādāsim ekovati vai akmensvati, izmantojot tam paredzēto aparātu.
            </ServicesBlock>
            <ServicesBlock 
                icon="brugesana.svg"
                heading="Bruģēšana un teritorijas labiekārtošana">
                    Nobruģēsim stāvvietu veikalam vai privātmājai. Celiņu bruģēšana, terašu būve, žogu montāža.
            </ServicesBlock>
            <ServicesBlock 
                icon="apzalumosana.svg"
                heading="Apzaļumošana">
                    Zaļa, skaista zāle visas teritorijas laukumā.
            </ServicesBlock>

        </div>

        <ActionBanner url='request-background.jpg' title="Piesaki projektu" subtitle="Konsultējies jebkurā jautājumā" cta="Pieteikties" href="email" />

        <div id="izcenojums" className="container mb0 mb6-ns">
            <Header color="yellow" showLine>Pakalpojumu Izcenojums</Header>
            <p className="mb5-ns">
                Visas cenas norādītas bez PVN <br/>
                <span className="red-text">Darba cenā nav iekļauti nodokļi, transporta un mehānismu izmaksas!</span>
            </p>

            <div className="cost-table">

                <div className="header flex flex-wrap b ms-bold justify-start mb3">

                    {
                        data.allCostsJson.edges.map((edge, id) => (
                            <div className="w-100 w-50-ns flex flex-column mb5">
                                <h3 className={id%2==0?"ms-bold":"ml5-ns ms-bold"}>{edge.node.category_title}</h3>

                                <div className="flex mb2-ns">
                                    <div className={id%2==0?"w-50 mb2":"w-50 mb2 ml5-ns"}>Pakalpojums</div>
                                    <div className={id%2==0?"w-50 mb2 tr pr5-ns":"w-50 mb2 tr pr0-ns"}>Izmaksas</div>
                                </div>

                                <div className="flex flex-wrap justify-around">
                                    {
                                        edge.node.items.map((item, id2) => {
                                            return (
                                                <div className="w-100 grow main-font cost-table-row mb3 pen">
                                                    <div className={id%2==0?"row-border flex mr5-ns pb1-ns":"row-border flex ml5-ns pb1-ns"}>
                                                        <div className="w-50 sec-font">{item.service}</div>
                                                        <div className="normal w-50 tr b sec-font">{item.cost}</div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        ))
                    }

                </div>

                    {/* <div className="w-50 flex">
                        <div className="w-50">Pakalpojums</div>
                        <div className="w-50 tr pr5-ns">Izmaksas</div>
                    </div>

                    <div className="w-50 flex">
                        <div className="w-50 pl5-ns">Pakalpojums</div>
                        <div className="w-50 tr">Izmaksas</div>
                    </div> */}
                {/* <div className="flex flex-wrap justify-around">
                    {
                        data.allCostsJson.edges.map((edge, id) => {
                            return (
                                <div className="w-50 grow main-font cost-table-row mb2">
                                    <div className={id%2==0?"row-border flex mr5-ns":"row-border flex ml5-ns"}>
                                        <div className="w-50">{edge.node.service}</div>
                                        <div className="blue w-50 tr b">{edge.node.cost}</div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div> */}


            </div>
        </div>

    </Layout>
    )
}

export default IndexPage


