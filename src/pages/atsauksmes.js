import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import PageHeader from "../components/navbar/header"
import SubmitTestimonial from "../components/testimonials/submitTestimonial"

import "./subpages.scss"

class Atsauksmes extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {

    return (
        
        <Layout>
            <SEO 
            title="Atstāj savu atsauksmi un vērtējumu" 
            description="Ja tev ir ko pateikt par mums, droši atstāj savu atsauksmi, norādot projektu, labo un slikto."
            />
            h2  
            <PageHeader heading="Atsauksmes" url="landing-background.jpg" />

            <div id="about" className="container mb5 mb6-ns">
                <SubmitTestimonial/>
            </div>

        </Layout>
    )
    }
}

export default Atsauksmes