import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../components/layout"
import Img from "gatsby-image"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import RigaImage from "../images/riga.png"
import "./subpages.scss"
import ActionBanner from "../components/action-banner/action-banner";
import PageHeader from "../components/navbar/header";
import StepNumber from "../components/objects/step-number"
import Logo from "../images/gatsby-icon_cropped.png"
import { Parallax } from 'react-parallax';

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout en>
        <SEO 
        title="About us and the projects"
        description="Lm Group Buve has been developing both large and small construction projects for more than 15 years."
        />
        
        
        <PageHeader heading="About us" url="about-background.jpg" />

        <div id="about" className="container mb5 mb6-l">
            <Header color="yellow" showLine>About LM GROUP BUVE</Header>
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-60-l">
                    <p className="pb4 pb5-ns db">
                    The company LM Group Buve was founded on January 10, 2005 and is currently carrying out both large and small construction projects in Riga and the Riga District for 15 years. We offer all solutions related to repairs and construction, mainly the construction of private houses. Our professional team of masters will take care that the repair or construction of an apartment or private house does not require you time, headaches and unnecessary funds.<br /><br />
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mb0-l">
                    <img style={{maxWidth: '70%'}} src={Logo} alt="LM GROUP BUVE logo" />
                </div>
            </div>

            <Link className="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib" paintDrip hex={transitionHex} to="/en/pakalpojumi">Services</Link>

        </div>

        <Parallax bgImage={RigaImage} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={0}>
            <div className="map relative pv5 pv4-ns pv6-l flex flex-column content-center w-100 container overflow-hidden">
            <div className="pv4">
                <h2 className="white normal ms-light mb0 pb0">Working within Riga, <br/> <span className="yellow">15km</span> - <span className="yellow">45km</span> and around Riga.</h2><br/>
                <p className="white ms-light mt0 mb0">If necessary, working withing the bounds of Latvia is possible</p>
            </div>
            </div>
        </Parallax>


        <div id="project-steps" className="container mb3">
            <Header color="yellow" showLine center>Project flow</Header>
            <div className="flex justify-center">
                <ol className="ml0 ph0 ph4-ns flex flex-column list db w-90 w-70-l">

                <StepNumber heading="Construction Project Review" number="1">
                    Once you have contacted us, we review the construction project, offering the prices indicated on the website (depending on the service). If everything satisfies you, we create a detailed estimate just for you. The estimate is very accurate and does not change during the work.
                </StepNumber>

                <StepNumber heading="Construction cost agreement" number="2">
                    If the construction costs are approved and an estimate is made, we discuss the payment procedure. It may be different in different projects and in different situations, so we will agree on it with you.
                </StepNumber>

                <StepNumber heading="Site preparation" number="3">
                    Preparations are needed before starting the project. We supply construction waste containers, builders' wagons and other household and construction needs.
                </StepNumber>

                <StepNumber heading="Construction process" number="4">
                    Let's start work! Every week we send pictures from the object, say general things about the progress of the project and problems, if any. In short - you will know what is going on and how far your project is!
                </StepNumber>

                <StepNumber heading="Object cleaning" number="5">
                    The object is completed, all construction debris has been collected, the object is tidy and clean. We remove the container, the car and everything else.
                </StepNumber>

            </ol>
            </div>
        </div>

        <ActionBanner url='request-background.jpg' title="Submit your project" subtitle="Consult about any questions" cta="Submit" href="email" />

        

        <div id="partners" className="container mb3 mb6-ns">
            <Header color="yellow" showLine>Partners</Header>
            <div className="flex flex-wrap">
                {
                    data.allPartnersJson.edges.map((edge, id) => {
                        return (
                            <div className="w-50 w-20-ns ph3" data-sal="fade" data-sal-delay={50*(id-1)} data-sal-duration="1000">
                                <Img fluid={edge.node.img.childImageSharp.fluid} alt={edge.node.name+" - logo"} />
                            </div>
                        )
                    })
                }
            </div>
        </div>

    </Layout>
  )
}

export default IndexPage
