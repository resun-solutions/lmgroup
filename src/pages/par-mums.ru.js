import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../components/layout"
import Img from "gatsby-image"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import RigaImage from "../images/riga.png"
import "./subpages.scss"
import ActionBanner from "../components/action-banner/action-banner";
import PageHeader from "../components/navbar/header";
import StepNumber from "../components/objects/step-number"
import Logo from "../images/gatsby-icon_cropped.png"
import { Parallax } from 'react-parallax';

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    {
      allPartnersJson {
        edges {
          node {
            link
            name
            img {
              publicURL
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  `);

  const transitionHex = "#0099ff";

  return (
    <Layout ru>
        <SEO 
        title="О компании и ходе реализации проекта"
        description="Lm Group Buve разрабатывает как крупные, так и мелкие строительные проекты более 15 лет."
        />
        
        
        <PageHeader heading="О нас" url="about-background.jpg" />

        <div id="about" className="container mb5 mb6-l">
            <Header color="yellow" showLine>O LM GROUP BUVE</Header>
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-60-l">
                    <p className="pb4 pb5-ns db">
                        LM Group Buve была основана 10 января 2005 года и уже 15 лет осуществляет как крупные, так и мелкие строительные проекты в Риге и Рижском районе. Мы предлагаем все решения, связанные с ремонтом и строительством, в основном строительство частных домов. Наша профессиональная команда мастеров позаботится о том, чтобы ремонт или строительство квартиры или частного дома не требовали от вас времени, головных болей и лишних средств.<br /><br />
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mb0-l">
                    <img style={{maxWidth: '70%'}} src={Logo} alt="LM GROUP BUVE logo" />
                </div>
            </div>

            <Link className="button button--blue w-100 w-auto-l tc cp br-10 pv3 ph4 relative dib" paintDrip hex={transitionHex} to="/pakalpojumi">Услуги</Link>

        </div>

        <Parallax bgImage={RigaImage} bgImageAlt="Attēls ar ēku un aprēķiniem" strength={0}>
            <div className="map relative pv5 pv4-ns pv6-l flex flex-column content-center w-100 container overflow-hidden">
            <div className="pv4">
                <h2 className="white normal ms-light mb0 pb0">Мы работаем в Риге, <br/> <span className="yellow">15kм</span> - <span className="yellow">45kм</span> вокруг Риги</h2><br/>
                <p className="white ms-light mt0 mb0">При необходимости мы выезжаем на объекты по всей стране</p>
            </div>
            </div>
        </Parallax>


        <div id="project-steps" className="container mb3">
            <Header color="yellow" showLine center>Этапы реализации проекта</Header>
            <div className="flex justify-center">
                <ol className="ml0 ph0 ph4-ns flex flex-column list db w-90 w-70-l">

                <StepNumber heading="Рассмотрение строительного проекта" number="1">
                    После того как вы свяжитесь с нами, мы рассмотрим проект, и в зависимости от услуги мы предложим вам цену, которая указана на сайте. Если вас все устроит, то мы рассчитаем точную смету, которая в процессе строительства не поменяется.
                </StepNumber>

                <StepNumber heading="Соглашение о стоимости строительства" number="2">
                    Если смета будет утверждена и создана, мы обсудим порядок оплаты. В разных проектах и разных ситуациях, порядок оплаты может отличаться, поэтому мы с вами об этом договоримся.
                </StepNumber>

                <StepNumber heading="Подготовка строительной площадки" number="3">
                    Перед тем как начать работы по строительству, необходимо провести подготовку строительной площадки, чтобы процесс стройки впоследствии был максимально эффективным. Мы предоставим контейнер для строительного мусора, строительный вагончик (бытовка) и другие необходимые бытовые и строительные нужны.
                </StepNumber>

                <StepNumber heading="Процесс строительства" number="4">
                    Начнём работу! Каждую неделю мы будем присылать вам фотографии с объекта, обговаривать этапы строительства и проблемы, если таковые будут. Одним словом - вы будете знать, что происходит на объекте и на каком он этапе.
                </StepNumber>

                <StepNumber heading="Уборка объекта" number="5">
                    Когда объект будет закончен, весь строительный мусор будет собран и территория будет чистой и убранной. А так же будет убран строительный контейнер, вагончик и все остальное.
                </StepNumber>

            </ol>
            </div>
        </div>

        <ActionBanner url='request-background.jpg' title="Подать заявку на проект" subtitle="Консультируйтесь по любому вопросу" cta="Продолжить" href="email" />

        

        <div id="partners" className="container mb3 mb6-ns">
            <Header color="yellow" showLine>Партнёры по сотрудничеству</Header>
            <div className="flex flex-wrap">
                {
                    data.allPartnersJson.edges.map((edge, id) => {
                        return (
                            <div className="w-50 w-20-ns ph3" data-sal="fade" data-sal-delay={50*(id-1)} data-sal-duration="1000">
                                <Img fluid={edge.node.img.childImageSharp.fluid} alt={edge.node.name+" - logo"} />
                            </div>
                        )
                    })
                }
            </div>
        </div>

    </Layout>
  )
}

export default IndexPage
