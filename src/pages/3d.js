import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import Img from "gatsby-image"
import PageHeader from "../components/navbar/header"
import ActionBanner from "../components/action-banner/action-banner"

import "./subpages.scss"

class threeD extends React.Component {

    constructor(props) {
        super(props);
    }

    render () {

    return (
    <Layout>
        <SEO 
        title="3D ideju vizualizācija" 
        description="Vai tev ir kāda ideja, kuru vēlies īstenot? Mēs to palīdzēsim pilnveidot, piedāvājot Tev vispiemērotāko risinājumu."
        />
        
        <PageHeader heading="3D vizualizācija" url="3d-background.png" />

        <div id="about" className="container mb6-ns mt0 mt5-ns ">
            <div className="flex flex-column flex-row-l ">
                <div className="w-100 w-60-l ">
                    <Header color="yellow" showLine>Tavas idejas 3D vizualizācija</Header>
                    <p className="db">
                    Ja tev ir kāda ideja, piemēram, pirts būvniecība, 3D vizualizācija tev palīdzēs saprast, kas ir tas, ko tiešām vēlies un vai tas, kā esi iztēlojies savā prātā, atbilst tam, kā izskatās dzīvē.
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mt4 mb0-l ">
                    <img data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500" style={{maxWidth: '70%'}} src={require('../images/3d.jpg')} alt="LM GROUP BUVE logo" />
                </div>
            </div>
        </div>

        <div className="flex flex-wrap">
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/1.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/2.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/3.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/5.png')}/>
            </div>

            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/6.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/7.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/9.png')}/>
            </div>
            <div className="w-100 w-50-l lh0">
                <img alt="3D vizualizācija" className="ma0 pa0 lh0 w-100" src={require('../images/3d/8.png')}/>
            </div>
        </div>

        <div className="flex flex-wrap container pb5 mb4-ns">
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="50" data-sal-duration="500">
                    <Header color="yellow" showLine>Izveide pēc plāna vai zīmējuma</Header>
                    <p className="db">
                        Mēs izveidosim modeli, kas atbilst Tavām prasībām un mērījumiem. Ja tev ir rasējums vai arī pats esi izmērījis un izveidojis zīmējumu, veiksim šī plāna 3d vizualizāciju.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="150" data-sal-duration="500">
                    <Header color="yellow" showLine>Reālistisks apgaismojums</Header>
                    <p className="db">
                        3D vizualizācija palīdzēs saprast, kā telpa izskatīsies dienas gaismā, rītausmā, vakara krēslā vai pat naktī.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center">
                <div className="wrapper w-70-ns" data-sal="fade-in" data-sal-delay="250" data-sal-duration="500">
                    <Header color="yellow" showLine>Interjera dizains</Header>
                    <p className="db">
                        Ar 3D palīdzību ir iespējams apskatīt, kā telpa izskatīsies dažādos toņos un attiecīgi izvēlēties vispiemērotāko krāsu un materiālu. Tas attiecas gan uz sienām, grīdu, gan arī mēbelēm un citiem elementiem.
                    </p>
                </div>
            </div>
            <div className="w-100 w-50-l flex justify-center" data-sal="fade-in" data-sal-delay="350" data-sal-duration="500">
                <div className="wrapper w-70-ns">
                    <Header color="yellow" showLine>Individuāli risinājumi</Header>
                    <p className="db">
                        Mēs esam saskārušies ar visdažādākajām problēmām un raduši atbildi uz visdažādākajiem jautājumiem.  Palīdzēsim tev realizēt savu ideju no sākuma līdz beigām.
                    </p>
                </div>
            </div>
        </div>

        <ActionBanner url='request-background.jpg' title="Piesakies vizualizācijai" subtitle="Mēs palīdzēsim Tev atrast vispiemērotāko risinājumu" cta="Pieteikties" href="email" />

        <div id="about" className="container mb5 mb6-ns flex items-center flex-column pt5 pt6-ns">
            <Header color="yellow" center>Mēbeles pēc pasūtījuma</Header>
            <div className="dib w-100 w-50-ns flex items-center flex-column">
                <p className="pb4 pb5-ns db mb0 tc">
                    Papildu 3D ideju vizualizācijai piedāvājam arī iebūvētu un korpusa mēbeļu izgatavošanu pēc individuālām vajadzībām.
                </p>
                <Link className="button w-100 w-auto-l tc button--blue cp br-10 pv3 ph4 relative dib mb0" to="/mebeles" data-sal="zoom-in" data-sal-delay="50" data-sal-duration="500">Uzzināt vairāk</Link>
            </div>

        </div>

    </Layout>
    )
    }
}

export default threeD

export const query = graphql`
{
  allProjectsJson {
    edges {
      node {
        work
        title
        img {
          publicURL
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`