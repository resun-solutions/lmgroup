import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Header from "../components/typography/header" // Title header
import SEO from "../components/seo"
import Img from "gatsby-image"
import PageHeader from "../components/navbar/header"
import CountUp from 'react-countup';
import StoreContext from "../context/storeContext"
import { Parallax } from 'react-parallax';
import Gallery from "../components/gallery/gallery.js"

import "./subpages.scss"

class IndexPage extends React.Component {

    isBottom(el) {
        return el.getBoundingClientRect().bottom <= window.innerHeight;
    }
  
    componentDidMount() {
        document.addEventListener('scroll', this.trackScrolling);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.trackScrolling);
    }


    trackScrolling = () => {
        const wrappedElement = document.getElementById('statistics');
        var bounding = wrappedElement.getBoundingClientRect();

        console.log(bounding.top);

        if ( bounding.top < 800 ) {
            setTimeout(() => {
                document.getElementById("statistics-start-1").click();
                document.getElementById("statistics-start-2").click();
                document.getElementById("statistics-start-3").click();
                document.getElementById("statistics-start-4").click();
            },500);
            document.removeEventListener('scroll', this.trackScrolling);
        }
    };

    render () {

    return (
        <>
    <StoreContext.Consumer>
      {
        storeData => (
            <Layout en>
                <SEO 
                title="List of completed projects" 
                description="LM Group Buve has implemented more than 200 amazing private house construction projects."
                />
                
                <PageHeader heading="PROJECTS" url="projects-background.jpg" />

                <div id="about" className="container mb5 mb6-ns">
                    <Header color="yellow" showLine>What projects do we carry out?</Header>
                    <p className="pb4 pb5-ns db">
                        The construction of private houses is our most developed service, but we also undertake apartment renovations, paving and other landscaping work, as well as the construction of shops and other buildings.
                    </p>
                    <button title="Kontakti" onClick={storeData.toggleEmailPopup} className="button w-100 w-auto-l button--blue cp br-10 pv3 ph4 relative dib" to="/projekti/#kontakti">Apply for a project</button>

                </div>

                <div className="flex flex-column-reverse flex-column-m flex-column-l">

                {/* Showcase */}
                <div id="showcase" className="flex flex-wrap lh0">

                    {
                    this.props.data.allProjectsJson.edges.slice(0).reverse().map((edge, id) => (
                        <div onClick={() => { storeData.toggleGallery(); storeData.setGalleryItemId(this.props.data.allProjectsJson.edges.slice(0).length - 1 - id) }} className="w-100 w-50-m w-20-l relative">
                            <Img fluid={edge.node.img.childImageSharp.fluid} alt="Projekta attēls" />
                            <div className="project-gradient flex flex-column justify-end pa3 white cp">
                                <span className="mh0 mb3 f6 ms-light o-70 lh1">
                                    {
                                    edge.node.work.map((item, id) => {
                                        if(id < edge.node.work.length-1) {
                                        return ""+item+" / "
                                        }else {
                                        return ""+item
                                        }
                                    })
                                    }
                                </span>
                                <h3 className="ma0 f5 normal ttu">{edge.node.title}</h3>
                            </div>
                        </div>
                        ))
                    }
                </div>
                {/* End Showcase */}

                <div id="statistics" className="container mv0 mv6-ns pv5 pv6-ns">

                    <div className="flex flex-column flex-row-m flex-row-l">

                        <div className="w-100 w-25-m w-25-l mb3 mb0-ns w-25-ns flex flex-column lh125">
                            <span className="blue f1 ms-black tc statistics-item">
                                <CountUp end={89} duration={4}>
                                {({ countUpRef, start }) => (
                                    <div>
                                    <span ref={countUpRef} />
                                    <button id="statistics-start-1"  className="absolute o-0 pen" onClick={start}>Start</button>
                                    </div>
                                )}
                                </CountUp>
                            </span>
                            <h2 className="f4 tc sec-font">Fill in the basics</h2>
                        </div>

                        <div className="w-100 w-25-m w-25-l mb3 mb0-ns w-25-ns flex flex-column lh125">
                            <span className="blue f1 tc ms-black statistics-item">
                                <CountUp end={200} duration={4}>
                                {({ countUpRef, start }) => (
                                    <div style={{display:'inline'}}>
                                    <span ref={countUpRef} />
                                    <button id="statistics-start-2"  className="absolute o-0 pen" onClick={start}>Start</button>
                                    </div>
                                )}
                                </CountUp>
                                +
                            </span>
                            <h2 className="f4 tc sec-font">Private houses</h2>
                        </div>
                        <div className="w-100 w-25-m w-25-l mb3 mb0-ns w-25-ns flex flex-column lh125">
                            <span className="blue f1 ms-black tc">
                            <span className="blue f1 ms-black tc statistics-item">
                                <CountUp end={70} duration={4}>
                                {({ countUpRef, start }) => (
                                    <div>
                                    <span ref={countUpRef} />
                                    <button id="statistics-start-3"  className="absolute o-0 pen" onClick={start}>Start</button>
                                    </div>
                                )}
                                </CountUp>
                            </span>
                            </span>
                            <h2 className="f4 tc sec-font">Installation of roof structures</h2>
                        </div>
                        <div className="w-100 w-25-m w-25-l flex flex-column lh125">
                            <span className="blue f1 ms-black tc">
                            <span className="blue f1 tc statistics-item">
                                <CountUp end={5000} duration={4}>
                                {({ countUpRef, start }) => (
                                    <div style={{display:'inline'}}>
                                    <span ref={countUpRef} />
                                    <button id="statistics-start-4"  className="absolute o-0 pen" onClick={start}>Start</button>
                                    </div>
                                )}
                                </CountUp>
                            </span> <small className="f4 relative m0 p0 main-font--bold">m2</small>
                            </span>
                            <h2 className="f4 tc sec-font">Paving</h2>
                        </div>
                    </div>
                
                </div>

                </div>

            </Layout>
        )
    }
    </StoreContext.Consumer>
    <Gallery projects={this.props.data.allProjectsJson.edges} />
    </>
    )
    }
}

export default IndexPage

export const query = graphql`
{
  allProjectsJson {
    edges {
      node {
        work
        title
        gallerySize
        img {
          publicURL
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`