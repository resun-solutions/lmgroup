module.exports = {
  siteMetadata: {
    title: `LM Group Buve`,
    description: `LM GROUP BUVE ir Latvijā konkurējošs būvuzņēmums, kas jau vairāk 15 gadus īsteno gan lielus, gan mazus projektus dažādās būvniecības sfērās.`,
    author: `Resun Solutions`,
    siteUrl: `https://www.lmgroup.lv`,
  },
  plugins: [
    `gatsby-transformer-sharp`,
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: false,
        stripMetadata: true,
        defaultQuality: 75,
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-transition-link`,
    `gatsby-plugin-scroll-reveal`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/data`,
      },
    },
    {
      resolve: 'gatsby-plugin-i18n',
      options: {        
        langKeyDefault: 'lv',
        prefixDefault: false,
        useLangKeyLayout: false
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `LM Group Buve`,
        short_name: `LM Group Buve`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#000000`,
        display: `minimal-ui`,
        icon: `src/images/favicon.jpg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-151320523-4",
        head: false,
        anonymize: false,
        pageTransitionDelay: 0,
        cookieDomain: "/",
      },
    },
    `gatsby-plugin-offline`
  ],
}
