import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import "./grozs.scss"
import StoreContext from "../../context/storeContext"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)

const ShopPage = () => {

  const clearCart = () => {
    if( window.confirm("Viss groza saturs tiks izdzēsts. Turpināt?") ) {
        localStorage.setItem('cart',JSON.stringify([]))
        window.location = '/veikals/gazbetona-bloki';
    }
  }

  let totalAmount = 0;

  return (
    <StoreContext.Consumer>
    {
      storeData => (
        <Layout minimal>
            <div className="flex">
              <div className="w-third flex items-center">
                <Link to="/veikals" className="ml5 dn db-ns">
                  <FontAwesomeIcon icon={['fas','chevron-left']}/>
                  <span className="ml2">Atgriezties veikalā</span>
                  </Link>
              </div>
              <div className="w-100 w-third-ns">
                <h1 className="f3 tc pt3 pb0 mt2 normal ms-bold">Tavs Grozs</h1>
                <p className="tc f6">Cenas aprēķinātas, tajās iekļaujot PVN</p>
              </div>
              <div className="w-third"></div>
            </div>
                <hr />
                <div className="cart-items-wrapper pl2 pr2 container">
                    {

                        Object.keys(storeData.cart).map((cartItem, index) => {
                          let itemLoc = storeData.cart[cartItem]['data'];

                          let dx = itemLoc.dx;
                          let dy = itemLoc.dy;
                          let dz = itemLoc.dz;
                          let cost = itemLoc.cost;
                          let density = null;
                          density = itemLoc.density;
                          

                          let idLoc = storeData.cart[cartItem]['id'];

                          totalAmount = totalAmount+parseInt(cost*storeData.cart[cartItem]['amount'])

                          if(density != null) {
                            return (
                                <div className="cart-item flex items-center mh0 mh6-l ph2 ph6-l mb2 justify-between">
                                    <h3 className="ma0">AeroBlock D{density} (
                                      {dx}x
                                      {dy}x
                                      {dz}) | Paletes: {storeData.cart[cartItem]['amount']} ({storeData.cart[cartItem]['amount']*1.8}m3)
                                    </h3>
                                    <span className="f6 cart-product-cost main-f">{parseInt(cost*storeData.cart[cartItem]['amount'])+0.21*parseInt(cost*storeData.cart[cartItem]['amount'])} EUR</span>
                                </div>
                            )  

                          } else {
                            return (
                              <div className="cart-item flex items-center mh0 mh6-l ph2 ph6-l mb2 justify-between">
                                  <h3 className ="ma0">Gāzbetona bloku pārsedze (
                                    {dx}x
                                    {dy}x
                                    {dz}) | Paletes: {storeData.cart[cartItem]['amount']} ({storeData.cart[cartItem]['amount']*1.8}m3)
                                  </h3>
                                  <span className="f6 cart-product-cost main-f">{parseInt(cost*storeData.cart[cartItem]['amount'])+0.21*parseInt(cost*storeData.cart[cartItem]['amount'])} EUR</span>
                              </div>
                            )  
                          }

                        })
                    }
                    <h3 className="tr f5 mh0 mh7-l">Kopā: {totalAmount+(0.21*totalAmount)}€</h3>
                    <div className="flex mh6-l ph6-l">
                      <button onClick={clearCart} className="cp button-1 no-border button--gray pv3 flex justify-center no-underline mt4 items-center fg sec-font w-50">Notīrīt</button>
                      <Link to="veikals/pasutijuma-noformesana" className="button-1 button--blue pv3 flex justify-center no-underline mt4 items-center fg sec-font w-50">
                          Turpināt
                          <FontAwesomeIcon className="ml2" icon={['fas', 'chevron-right']}  />
                      </Link>
                    </div>
                </div>


        </Layout>
      )
    }
    </StoreContext.Consumer>
  )
}

export default ShopPage
