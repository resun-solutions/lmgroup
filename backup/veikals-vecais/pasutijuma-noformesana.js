import React, {useState, useEffect} from "react"
import StoreContext from "../../context/storeContext"
import { Link, graphql, useStaticQuery } from "gatsby"
import "./pasutijuma-noformesana.scss"

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)

const Checkout = () => {
    
    const data = useStaticQuery(graphql`
    {
      allProductsJson {
        edges {
          node {
            path
            products {
              productId
              dx
              dy
              dz
              density
            }
          }
        }
      }
    }
  `);

    const formSubmit = () => {
        alert("Pasūtījums tika nosūtīts. Ja ar Jums nesazināmies 24h laikā, zvaniet vai rakstiet mums!");
        localStorage.setItem('cart',JSON.stringify([]))
        
        setTimeout(() =>{
            window.location = "/";
        },3000);
    }

   // let orderItems = JSON.parse(localStorage.getItem('cart'));

    return (
        <StoreContext.Consumer>
        {
        storeData => {

            let orderData = Object.keys(storeData.cart).map(index=>{
                
                let itemData = storeData.cart[index]['data'];
                let prodDescr = null;

                if(itemData.density != null) {
                    prodDescr = "Produkta Nosaukums: AeroBlock D"+itemData['density']+" ("+itemData['dx']+"x"+itemData['dy']+"x"+itemData['dz']+") | Paletes: "+storeData.cart[index]['amount']
                }else {
                    prodDescr = "Produkta Nosaukums: Gāzbetona bloku pārsedze ("+itemData['dx']+"x"+itemData['dy']+"x"+itemData['dz']+") | Paletes: "+storeData.cart[index]['amount'];
                }
                
                return (
                    prodDescr
                )
            });

            return (
                <main className="checkout-section">
                    <h1 className="f3 tc pt4 pb2 normal">Pasūtījuma noformēšana</h1>
                    <hr />
                    <form onSubmit={formSubmit} target="_blank" method="POST" action="https://chalete.lv/es/index.php" className="checkout-form pl3 pr3 mb3 main-f">
                        <div className="input-group flex flex-column mb3">
                            <label className="b pb2">Vārds</label>
                            <input minLength="3" className="pl3 pr3 pt2 pb2" type="text" name="name" placeholder="Vārds" />
                        </div>
                        <div className="input-group flex flex-column mb3">
                            <label className="b pb2">Uzvārds</label>
                            <input minLength="3" className="pl3 pr3 pt2 pb2" type="text" name="last_name" placeholder="Uzārds" />
                        </div>
                        <div className="input-group flex flex-column mb3">
                            <label className="b pb2">E-pasts</label>
                            <input minLength="3" className="pl3 pr3 pt2 pb2" type="email" name="email" placeholder="E-pasts" />
                        </div>
                        <div className="input-group flex flex-column mb3">
                            <label className="b pb2">Tel. Nr.</label>
                            <input minLength="3" className="pl3 pr3 pt2 pb2" type="phone" name="phone" placeholder="Tel. Nr." />
                        </div>
                        <div className="input-group flex flex-column mb3">
                            <label className="b pb2">Piegādes Adrese</label>
                            <input minLength="5" className="pl3 pr3 pt2 pb2" type="text" name="address" placeholder="Piegādes Adrese" />
                        </div>

                        <input type="text" className="dn" value="882hdhd" name="token" />
                        <input type="text" className="dn" value={JSON.stringify(orderData)} name="order" />

                        <span className="checkout-hint tc db">Mēs nosūtīsim rēķinu uz norādīto e-pastu. Kad tas tiks apmaksāts, nosūtīsim produktu uz norādīto adresi.</span>
                        <button className="w-100 pv3 button-1 no-border pv2 ph3 sec-font button--blue mt3 checkout-submit normal pointer" type="submit">Pasūtīt <FontAwesomeIcon className="ml2" icon={['fas', 'lock']}  /></button>
                    </form>
                </main>
            )
        }

        }
        </StoreContext.Consumer>
    )
}

export default Checkout