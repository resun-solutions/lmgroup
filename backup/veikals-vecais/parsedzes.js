import React from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import Layout from "../../components/layout"
import Header from "../../components/typography/header" // Title header
import SEO from "../../components/seo"
import "../subpages.scss"
import ActionBanner from "../../components/action-banner/action-banner";
import PageHeader from "../../components/navbar/header";
import StepNumber from "../../components/objects/step-number"
import StoreWrapper from "../../components/store/wrapper"
import Calculator from "../../components/calculator/calculator.js"
import ProductThumbnail from "../../images/store/1/parsedze.png"

const ShopPage = () => {

  return (
    <Layout>
        <SEO 
        title="Par uzņēmumu un projektu gaitu"
        description="Lm Group Buve jau vairāk kā 15 gadus izstrādā gan lielus gan mazus būvniecības projektus."
        />
        
        
        <PageHeader heading="E-Veikals" url="about-background.jpg" />

        <div id="shop" className="container mb5 mb6-l">
            <Header color ="yellow" showLine>Augstas kvalitātes pārsedzes</Header>
            <div className="flex flex-column flex-row-l">
                <div className="w-100 w-60-l">
                    <p className="pb0 pb0-ns db">
                      Gāzbetona pārsedzes Latvijā jau ražojam 5 gadus,esam pilnveidojuši un optimizējuši ražošanas procesus, iegūstot krietni izturīgākas un kvalitatīvākas gatavās gāzbetona pārsedzes. <br/>
                      <br/>
                      Gatavās gāzbetona pārsedzes krietni atvieglo būvniecības procesu, tādējādi samazinot liekas izmaksas uz darbu un netērējot lieku laiku būvniecībā, veidojot pašiem pārsedzes no U Blokiem vai monolīta. Gatavās gāzbetona pārsedzes izmaksu ziņā neatpaliek no izmaksām, ja veido uz vietas objektā, no U Blokiem jeb veidņos armētas ar betonu ielietas
                    </p>

                    <hr/>
                    <p className="b">
                      Pēc pieprasījuma ir iespējams iegādāties visu, kas nepieciešams lai uzbūvētu privātmāju. Ir pieejami arī visa veida bloki - ytong/bauroc/rocklite/fibo.
                    </p>
                </div>
                <div className="w-100 w-40-l flex items-center justify-center mb5 mb0-l">
                    <img alt="Gāzbetona bloks" className="" style={{maxWidth: '100%'}} src={ProductThumbnail} />
                </div>
            </div>

            <div className="mt3 mt6-l">
              <Calculator version={1} />
            </div>

        </div>


    </Layout>
  )
}

export default ShopPage
