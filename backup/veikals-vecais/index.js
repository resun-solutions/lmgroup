import React from "react"
import { Redirect } from '@reach/router'

const ShopPage = () => {

  return (
    <Redirect noThrow to="/veikals/gazbetona-bloki" />
  )
}

export default ShopPage
